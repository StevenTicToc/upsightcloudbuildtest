/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System.Collections.Generic;
using UpsightMiniJSON;

public class UpsightContentAttributes
{
    public Type ContentType { get; private set; }
    public string ContentProperties { get; private set; }


    public static UpsightContentAttributes FromJson(string json, out string scope)
    {
        scope = null;
        var content = new UpsightContentAttributes();
        var dict = Json.ToJsonObject(json);
        if(dict != null)
        {
            if(dict.ContainsKey("scope"))
                scope = dict["scope"].ToString();

            if(dict.ContainsKey("type"))
                content.ContentType = (Type)dict.GetPrimitive<int>("type");

            if(dict.ContainsKey("properties"))
                content.ContentProperties = Json.Serialize(dict["properties"]);
        }
        return content;
    }

    public override string ToString()
    {
        return string.Format("[UpsightContentAttributes] ContentType: {0}, ContentProperties: {1}", ContentType, ContentProperties);
    }

    //Must match com.upsight.android.marketing.UpsightContentHint.Type
    public enum Type
    {
        UNKNOWN,
        ANNOUNCEMENT,
        INTERNAL_CROSS_PROMOTION,
        REWARDS,
        VIRTUAL_GOODS_PROMOTION,
        OPT_IN,
        ADS,
        MORE_GAMES,
        VIDEO_CAMPAIGN,
        CUSTOM_VIEW,
        MEDIATION,
    }
}
