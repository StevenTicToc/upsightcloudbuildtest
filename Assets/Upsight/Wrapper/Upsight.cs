/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

#define UPSIGHT_AUTO_START

using UnityEngine;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UpsightMiniJSON;
using DateTime = System.DateTime;
using TimeSpan = System.TimeSpan;
using DateTimeKind = System.DateTimeKind;

public enum UpsightLoggerLevel
{
#if UNITY_IOS
    Off = -1,
    Critical, // iOS only. Critical errors indicating the state when the system cannot continue to work.
#endif
    Error, // Used to indicate errors during normal workflow.
    Warn, // Used to indicate issues that might need attention.
    Info, // Used to indicate information that might be of interest in particular workflow.
    Debug, // Used to indicate information that might help to understand the overall state of the system, data workflow, etc.
    Verbose // Android only, equivalant to Debug on iOS
}

public enum UpsightPurchaseResolution
{
    Buy = 0,
    Cancel,
    Fraud,
    Refund
}

public enum UpsightUserResult
{
    NativeNotInitialized = -1,
    Success = 0,
    NullUserID,
    InvalidUserIDLength,
    ContentIsDisplayed
}

public enum UpsightEnhancedPrivacyApplicability
{
    Auto = 0,
    Yes = 1,
    No = 2,
}

/// <summary>Helpful extension functions.</summary>
public static class UpsightExtensions
{
    public static long ToUnixTimestamp(this DateTime dateTime)
    {
        var time = dateTime - UnixEpoch;
        long ts = time.Seconds + (time.Minutes * 60L) + (time.Hours * 60L * 60L) + (time.Days * 24L * 60L * 60L);
        return ts;
    }

    public static DateTime ToDateTime(this long timestamp)
    {
        TimeSpan span = System.TimeSpan.FromSeconds(timestamp);
        DateTime time = UnixEpoch + span;
        return time;
    }

    public static DateTime ToDateTime(this double timestamp)
    {
        TimeSpan span = System.TimeSpan.FromSeconds(timestamp);
        DateTime time = UnixEpoch + span;
        return time;
    }

    private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
}



public partial class Upsight
{
    private static bool Initialized = false;

#if UPSIGHT_AUTO_START
    static Upsight()
    {
        Upsight.init();
    }
#endif

#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern void _usSetUpsightManagerName(string managerName);
    /// <summary>
    /// This instantiates the plugin classes. Should be called immediately at app launch.
    /// </summary>
    public static void init()
    {
        if(Initialized)
            return;
        Initialized = true;
        UpsightManager.init();
        _usSetUpsightManagerName(UpsightManager.GameObjectName);
    }

    /// <summary>
    /// This resets the init state.
    /// This should only be called when disabling the SDK in order to prevent calls to native.
    /// </summary>
    internal static void terminate()
    {
        Initialized = false;
    }


    #region User Attributes


    [DllImport("__Internal")]
    private static extern int _usSetUserID(string id, bool preserve);

    /// <summary>
    /// Starts a new user session for a specified userID.
    /// </summary>
    /// <remarks>
    /// userID cannot be the empty string and has a max length of 256 bytes when encoded in UTF-8.
    /// You cannot explicitly log into the anonymous user.
    /// The user remain "logged in" between app starts.
    /// If there is no userID currently set, the user is tracked as an anonymous user.
    /// The anonymous user is treated as a regular user with data persisting between sessions.
    /// User Attributes are associated with a userID and will change when a different userID is set.
    /// Device sessions are independant from user sessions. Starting a new user session does not start a new device session.
    /// SWITCHING BETWEEN USERS:
    /// Logging into Existing User: Previous user info is saved and the user is "logged out" (including anonymous users). Existing user is "logged in".
    /// Existing User -> Anonymous User: Previous user info in saved and user is "logged out". Anonymous user is "logged in".
    /// Existing User -> New User: Previous user info in saved and user is "logged out". A new user is created and "logged in" using the default values.
    /// Anonymous User -> New User: The 'preserve' parameter determines whether the anonymous user data should be copied into the new user.
    /// If 'preserve' is false, the anonymous user is "logged out", a new user is created using default values and "logged in".
    /// If 'preserve' is true, the anonymous user data is copied over to to the newly created user, the new user is "logged in", and the anonymous user data is reset to default.
    /// </remarks>
    /// <param name="id">The id of the user that should be "logged in".</param>
    /// <param name="preserve">Determines the behaviour when switching from an anonymous user to a new user.</param>
    /// <returns>true if successful; false otherwise</returns>
    public static UpsightUserResult setUserID(string id, bool preserve)
    {
        if(InitSuccessful())
            return (UpsightUserResult) _usSetUserID(id, preserve);

        return UpsightUserResult.NativeNotInitialized;
    }


    [DllImport("__Internal")]
    private static extern int _usClearUserID();

    /// <summary>
    /// Ends a user session for the currently "logged in" user. The anonymous user is "logged in".
    /// </summary>
    public static UpsightUserResult clearUserID()
    {
        if(InitSuccessful())
            return (UpsightUserResult) _usClearUserID();

        return UpsightUserResult.NativeNotInitialized;
    }


    [DllImport("__Internal")]
    private static extern int _usDeleteUser(string id);

    /// <summary>
    /// Removes all data stored for the specified userID. You cannot delete the anonymous user.
    /// </summary>
    /// <param name="id">The id of the user to delete.</param>
    public static UpsightUserResult deleteUser(string id)
    {
        if(InitSuccessful())
           return (UpsightUserResult) _usDeleteUser(id);

        return UpsightUserResult.NativeNotInitialized;
    }


    [DllImport("__Internal")]
    private static extern string _usGetCurrentUserID();

    /// <summary>
    /// Returns the id of the currently "logged in" user.
    /// </summary>
    /// <returns>The current userID.</returns>
    public static string getCurrentUserID()
    {
        if(InitSuccessful())
            return _usGetCurrentUserID();

        return null;
    }


    [DllImport("__Internal")]
    private static extern int _usGetCurrentUserSessionNumber();

    /// <summary>
    /// Returns the session number of the currently "logged in" user.
    /// </summary>
    /// <returns>The session number for the current user.</returns>
    public static int getCurrentUserSessionNumber()
    {
        if(InitSuccessful())
            return _usGetCurrentUserSessionNumber();

        return -1;
    }


    [DllImport("__Internal")]
    private static extern long _usGetCurrentUserSessionStartTime();

    /// <summary>
    /// Returns the start time of the session for the currently "logged in" user.
    /// </summary>
    /// <returns>The start time of the current user session.</returns>
    public static DateTime getCurrentUserSessionStartTime()
    {
        if(InitSuccessful())
        {
            return _usGetCurrentUserSessionStartTime().ToDateTime();
        }

        return default(DateTime);
    }


    [DllImport("__Internal")]
    private static extern void _usResetUserAttributes();

    /// <summary>
    /// Resets user attributes to the default values for the currently "logged in" user.
    /// </summary>
    public static void resetUserAttributes()
    {
        if(InitSuccessful())
            _usResetUserAttributes();
    }


    [DllImport("__Internal")]
    private static extern void _usSetUserAttributesString(string key, string value);

    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeString(string key, string value)
    {
        if(InitSuccessful())
            _usSetUserAttributesString(key, value);
    }


    [DllImport("__Internal")]
    private static extern void _usSetUserAttributesFloat(string key, float value);

    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeFloat(string key, float value)
    {
        if(InitSuccessful())
            _usSetUserAttributesFloat(key, value);
    }


    [DllImport("__Internal")]
    private static extern void _usSetUserAttributesInt(string key, int value);

    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeInt(string key, int value)
    {
        if(InitSuccessful())
            _usSetUserAttributesInt(key, value);
    }


    [DllImport("__Internal")]
    private static extern void _usSetUserAttributesBool(string key, bool value);

    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeBool(string key, bool value)
    {
        if(InitSuccessful())
            _usSetUserAttributesBool(key, value);
    }


    [DllImport("__Internal")]
    private static extern void _usSetUserAttributesDate(string key, double value);

    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeDate(string key, DateTime value)
    {
        if(InitSuccessful())
            _usSetUserAttributesDate(key, value.ToUnixTimestamp());
    }


    [DllImport("__Internal")]
    private static extern void _usSetShouldSynchronizeManagedVariables(bool value);

    /// <summary>
    /// Sets a boolean flag indicating if the SDK should apply Managed Variable update. By default varibles are synchronized.
    /// </summary>
    /// <param name="value">Value.</param>
    public static void setShouldSynchronizeManagedVariables(bool value)
    {
        if(InitSuccessful())
            _usSetShouldSynchronizeManagedVariables(value);
    }


    [DllImport("__Internal")]
    private static extern string _usGetUserAttributesString(string key);

    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes string.</returns>
    /// <param name="key">Key.</param>
    public static string getUserAttributeString(string key)
    {
        if(InitSuccessful())
            return _usGetUserAttributesString(key);

        return null;
    }


    [DllImport("__Internal")]
    private static extern float _usGetUserAttributesFloat(string key);

    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes string.</returns>
    /// <param name="key">Key.</param>
    public static float getUserAttributeFloat(string key)
    {
        if(InitSuccessful())
            return _usGetUserAttributesFloat(key);

        return 0f;
    }


    [DllImport("__Internal")]
    private static extern int _usGetUserAttributesInt(string key);

    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes string.</returns>
    /// <param name="key">Key.</param>
    public static int getUserAttributeInt(string key)
    {
        if(InitSuccessful())
            return _usGetUserAttributesInt(key);

        return 0;
    }


    [DllImport("__Internal")]
    private static extern bool _usGetUserAttributesBool(string key);

    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes string.</returns>
    /// <param name="key">Key.</param>
    public static bool getUserAttributeBool(string key)
    {
        if(InitSuccessful())
            return _usGetUserAttributesBool(key);

        return false;
    }


    [DllImport("__Internal")]
    private static extern double _usGetUserAttributesDate(string key);

    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes string.</returns>
    /// <param name="key">Key.</param>
    public static DateTime getUserAttributeDate(string key)
    {
        if(InitSuccessful())
            return _usGetUserAttributesDate(key).ToDateTime();

        return default(DateTime);
    }

    #endregion

    #region Enhanced Privacy

    [DllImport("__Internal")]
    private static extern bool _usSetCurrentCountry(string countryCode);

    /// <summary>
    /// Sets the user's current country
    /// </summary>
    /// <param name="countryCode">Country Code.</param>
    public static bool setCurrentCountry(string countryCode) {
        if(InitSuccessful())
            return _usSetCurrentCountry(countryCode);

        return false;
    }

    [DllImport("__Internal")]
    private static extern string _usGetCurrentCountry();

    /// <summary>
    /// Gets the user's current country
    /// </summary>
    /// <returns>The user current country string.</returns>
    public static string getCurrentCountry() {
        if(InitSuccessful())
            return _usGetCurrentCountry();

        return null;
    }

    [DllImport("__Internal")]
    private static extern bool _usSetHomeCountry(string countryCode);

    /// <summary>
    /// Sets the user's home country
    /// </summary>
    /// <param name="countryCode">Country Code.</param>
    public static bool setHomeCountry(string countryCode) {
        if(InitSuccessful())
            return _usSetHomeCountry(countryCode);

        return false;
    }

    [DllImport("__Internal")]
    private static extern string _usGetHomeCountry();

    /// <summary>
    /// Gets the user's home country
    /// </summary>
    /// <returns>The user home country string.</returns>
    public static string getHomeCountry() {
        if(InitSuccessful())
            return _usGetHomeCountry();

        return null;
    }

    [DllImport("__Internal")]
    private static extern void _usSetEnhancedPrivacyApplicability(int level);

    /// <summary>
    /// Sets the user's enhanced privacy applicability
    /// </summary>
    /// <param name="level">True if enhance privacy applies to the user. False if it does not. Null to let the Upsight platform decide</param>
    public static void setEnhancedPrivacyApplicability(UpsightEnhancedPrivacyApplicability level) {
        if(InitSuccessful())
            _usSetEnhancedPrivacyApplicability((int)level);
    }

    [DllImport("__Internal")]
    private static extern int _usGetEnhancedPrivacyApplicability();

    /// <summary>
    /// Gets user's enhanced privacy applicability level
    /// </summary>
    /// <returns>The user's enhanced privacy applicability level.</returns>
    public static UpsightEnhancedPrivacyApplicability getEnhancedPrivacyApplicability() {
        if(InitSuccessful())
            return (UpsightEnhancedPrivacyApplicability)_usGetEnhancedPrivacyApplicability();

        return UpsightEnhancedPrivacyApplicability.Auto;
    }

    [DllImport("__Internal")]
    private static extern void _usSetEnhancedPrivacyConsentGranted(bool granted);

    /// <summary>
    /// Sets the user's enhanced privacy consent
    /// </summary>
    /// <param name="granted">Consent granted.</param>
    public static void setEnhancedPrivacyConsentGranted(bool granted) {
        if(InitSuccessful())
            _usSetEnhancedPrivacyConsentGranted(granted);
    }

    [DllImport("__Internal")]
    private static extern bool _usIsEnhancedPrivacyConsentGranted();

    /// <summary>
    /// Gets whether the user has granted consent for enhanced privacy
    /// </summary>
    /// <returns>The user's enhanced privacy consent state.</returns>
    public static bool isEnhancedPrivacyConsentGranted() {
        if(InitSuccessful())
            return _usIsEnhancedPrivacyConsentGranted();

        return false;
    }

    #endregion

    #region Events and Milestones

    [DllImport("__Internal")]
    private static extern void _usRecordSessionlessAnalyticsEvent(string eventName, string properties);

    /// <summary>
    /// Reports a custom event not associated with a particular session
    /// </summary>
    /// <param name="eventName">Custom event name.</param>
    /// <param name="properties">Properties.</param>
    public static void recordSessionlessCustomEvent(string eventName, Dictionary<string, object> properties = null)
    {
        if(InitSuccessful())
            _usRecordSessionlessAnalyticsEvent(eventName, properties != null ? Json.Serialize(properties) : null);
    }

	[DllImport("__Internal")]
	private static extern void _usRecordSessionlessAnalyticsEventInt(string eventName,int value, string properties);
	[DllImport("__Internal")]
	private static extern void _usRecordSessionlessAnalyticsEventDouble(string eventName,double value, string properties);

	/// <summary>
	/// Reports a custom event not associated with a particular session
	/// </summary>
	/// <param name="eventName">Custom event name.</param>
	/// <param name="Value">Value of the event.</param>
	/// <param name="properties">Properties.</param>
	public static void recordSessionlessCustomEvent(string eventName, int value, Dictionary<string, object> properties = null)
	{
	if(InitSuccessful())
	_usRecordSessionlessAnalyticsEventInt(eventName, value, properties != null ? Json.Serialize(properties) : null);
	}
	public static void recordSessionlessCustomEvent(string eventName, double value, Dictionary<string, object> properties = null)
	{
	if(InitSuccessful())
	_usRecordSessionlessAnalyticsEventDouble(eventName, value, properties != null ? Json.Serialize(properties) : null);
	}


    [DllImport("__Internal")]
    private static extern void _usRecordAnalyticsEvent(string eventName, string properties);

    /// <summary>
    /// Reports a custom event
    /// </summary>
    /// <param name="eventName">Custom event name.</param>
    /// <param name="properties">Properties.</param>
    public static void recordCustomEvent(string eventName, Dictionary<string, object> properties = null)
    {
        if(InitSuccessful())
            _usRecordAnalyticsEvent(eventName, properties != null ? Json.Serialize(properties) : null);
    }

	[DllImport("__Internal")]
	private static extern void _usRecordAnalyticsEventInt(string eventName,int value, string properties);
	[DllImport("__Internal")]
	private static extern void _usRecordAnalyticsEventDouble(string eventName,double value, string properties);

	/// <summary>
	/// Reports a custom event
	/// </summary>
	/// <param name="eventName">Custom event name.</param>
	/// <param name="Value">Value of the event.</param>
	/// <param name="properties">Properties.</param>
	public static void recordCustomEvent(string eventName,int value, Dictionary<string, object> properties = null)
	{
	if(InitSuccessful())
	_usRecordAnalyticsEventInt(eventName, value, properties != null ? Json.Serialize(properties) : null);
	}
	public static void recordCustomEvent(string eventName, double value, Dictionary<string, object> properties = null)
	{
	if(InitSuccessful())
	_usRecordAnalyticsEventDouble(eventName, value, properties != null ? Json.Serialize(properties) : null);
	}


    [DllImport("__Internal")]
    private static extern void _usRecordMilestoneEvent(string scope, string properties);

    /// <summary>
    /// Reports a milestone event for the given scope
    /// </summary>
    /// <param name="scope">Scope.</param>
    public static void recordMilestoneEvent(string scope, Dictionary<string, object> properties = null)
    {
        if(InitSuccessful())
            _usRecordMilestoneEvent(scope, properties != null ? Json.Serialize(properties) : null);
    }


    [DllImport("__Internal")]
    private static extern void _usPrepareBillboardForScope(string scope);

    /// <summary>
    /// Prepares a billboard for display
    /// </summary>
    /// <param name="scope">Scope.</param>
    public static void prepareBillboard(string scope)
    {
        if(InitSuccessful())
            _usPrepareBillboardForScope(scope);
    }


    [DllImport("__Internal")]
    private static extern void _usDestroyBillboard(string scope);

    /// <summary>
    /// Destroys the loaded billboard for the given scope
    /// </summary>
    public static void destroyBillboard(string scope)
    {
        if(InitSuccessful())
            _usDestroyBillboard(scope);
    }


    [DllImport("__Internal")]
    private static extern bool _usIsContentReadyForBillboardWithScope(string scope);

    /// <summary>
    /// Indicates if there is content available for the billboard with a given scope.
    /// </summary>
    public static bool isContentReadyForBillboardWithScope(string scope)
    {
        if(InitSuccessful())
            return _usIsContentReadyForBillboardWithScope(scope);

        return false;
    }


    [DllImport("__Internal")]
    private static extern void _usRecordMonetizationEvent(string currency, string product, double totalPrice, int resolution, string properties);

    /// <summary>
    /// Records a monetization event
    /// </summary>
    /// <param name="totalPrice">Total price.</param>
    /// <param name="currency">Currency.</param>
    /// <param name="resolution">Resolution.</param>
    /// <param name="product">Product.</param>
    /// <param name="price">Price.</param>
    /// <param name="quantity">Quantity.</param>
    /// <param name="properties">Properties.</param>
    public static void recordMonetizationEvent(double totalPrice, string currency, UpsightPurchaseResolution resolution, string product = null, double price = -1f, int quantity = -1, Dictionary<string, object> properties = null)
    {
        if(InitSuccessful())
            _usRecordMonetizationEvent(currency, product, totalPrice, (int)resolution, properties != null ? Json.Serialize(properties) : null);
    }


    /// <summary>
    /// Android only. Records a purchase.
    /// </summary>
    /// <param name="quantity">Quantity.</param>
    /// <param name="currency">Currency.</param>
    /// <param name="price">Price.</param>
    /// <param name="totalPrice">Total price.</param>
    /// <param name="product">Product.</param>
    /// <param name="responseCode">Response code.</param>
    /// <param name="inAppPurchaseData">In-app purchase data.</param>
    /// <param name="inAppDataSignature">In-app data signature.</param>
    /// <param name="properties">Properties.</param>
    public static void recordGooglePlayPurchase(int quantity, string currency, double price, double totalPrice, string product, int responseCode, string inAppPurchaseData, string inAppDataSignature, Dictionary<string, object> properties = null)
    { }


    [DllImport("__Internal")]
    private static extern void _usRecordAppleStorePurchase(int quantity, string currency, double price, string transactionIdentifier, string product, int resolution, string properties);

    /// <summary>
    /// iOS only. Records the Apple store purchase
    /// </summary>
    /// <param name="quantity">Quantity.</param>
    /// <param name="currency">Currency.</param>
    /// <param name="price">Price.</param>
    /// <param name="transactionIdentifier">Transaction identifier.</param>
    /// <param name="product">Product.</param>
    /// <param name="resolution">Resolution.</param>
    /// <param name="properties">Properties.</param>
    public static void recordAppleStorePurchase(int quantity, string currency, double price, string transactionIdentifier, string product, UpsightPurchaseResolution resolution, Dictionary<string, object> properties = null)
    {
        if(InitSuccessful())
            _usRecordAppleStorePurchase(quantity, currency, price, transactionIdentifier, product, (int)resolution, properties != null ? Json.Serialize(properties) : null);
    }


    [DllImport("__Internal")]
    private static extern void _usRecordAttributionEvent(string campaign, string creative, string source, string properties);

    /// <summary>
    /// An event that notifies the server of the attribution source for a new install.
    /// </summary>
    /// <param name="campaign">The campaign ID for this creative.</param>
    /// <param name="creative">The ID for the creative receiving attribution.</param>
    /// <param name="source">The network source for the creative.</param>
    /// <param name="properties">A dictionary containing any information you would like to associate with this event.</param>
    public static void recordAttributionEvent(string campaign, string creative, string source, Dictionary<string, object> properties = null)
    {
        if(InitSuccessful())
            _usRecordAttributionEvent(campaign, creative, source, properties != null ? Json.Serialize(properties) : null);
    }

    #endregion

    #region Push Notifications

    [DllImport("__Internal")]
    private static extern void _usRegisterForPushNotifications();

    /// <summary>
    /// Registers the current device for push notifications.
    /// </summary>
    public static void registerForPushNotifications()
    {
        if(InitSuccessful())
            _usRegisterForPushNotifications();
    }


    [DllImport("__Internal")]
    private static extern void _usUnregisterForPushNotifications();

    /// <summary>
    /// Unregisters the current device for push notifications.
    /// </summary>
    public static void unregisterForPushNotifications()
    {
        if(InitSuccessful())
            _usUnregisterForPushNotifications();
    }

    #endregion

    #region UXM

    [DllImport("__Internal")]
    private static extern string _usGetManagedStringWithTag(string tag);

    /// <summary>
    /// Gets the Managed String identified by the tag
    /// </summary>
    /// <returns>The current value of the managed string.</returns>
    /// <param name="tag">The unique identifier of the string whose value you want to retrieve.</param>
    public static string getManagedString(string tag)
    {
        if(InitSuccessful())
            return _usGetManagedStringWithTag(tag);

        return null;
    }


    [DllImport("__Internal")]
    private static extern float _usGetManagedFloatWithTag(string tag);

    /// <summary>
    /// Gets the Managed Float identified by the tag
    /// </summary>
    /// <returns>The current value of the managed float.</returns>
    /// <param name="tag">The unique identifier of the float whose value you want to retrieve.</param>
    public static float getManagedFloat(string tag)
    {
        if(InitSuccessful())
            return _usGetManagedFloatWithTag(tag);

        return 0f;
    }


    [DllImport("__Internal")]
    private static extern int _usGetManagedIntWithTag(string tag);

    /// <summary>
    /// Gets the managed int identified by the tag
    /// </summary>
    /// <returns>The current value of the managed int.</returns>
    /// <param name="tag">The unique identifier of the int whose value you want to retrieve.</param>
    public static int getManagedInt(string tag)
    {
        if(InitSuccessful())
            return _usGetManagedIntWithTag(tag);

        return 0;
    }


    [DllImport("__Internal")]
    private static extern bool _usGetManagedBoolWithTag(string tag);

    /// <summary>
    /// Gets the managed bool identified by the tag
    /// </summary>
    /// <returns>The current value of the managed bool.</returns>
    /// <param name="tag">The unique identifier of the bool whose value you want to retrieve.</param>
    public static bool getManagedBool(string tag)
    {
        if(InitSuccessful())
            return _usGetManagedBoolWithTag(tag);

        return false;
    }

    [DllImport("__Internal")]
    private static extern int _usGetLatestSessionNumber();

    /// <summary>
    /// Gets session number of the latest Upsight session.
    /// </summary>
    /// <returns>The session number.</returns>
    public static int getLatestSessionNumber()
    {
        if(InitSuccessful())
            return _usGetLatestSessionNumber();

        return 0;
    }

    [DllImport("__Internal")]
    private static extern long _usGetLatestSessionStartTimestamp();

    /// <summary>
    /// Gets session start timestamp of the latest Upsight session.
    /// </summary>
    /// <returns>The session start timestamp.</returns>
    public static long getLatestSessionStartTimestamp()
    {
        if(InitSuccessful())
            return _usGetLatestSessionStartTimestamp();

        return 0L;
    }

    #endregion

    #region Miscellaneous

    [DllImport("__Internal")]
    private static extern string _usGetAppToken();

    /// <summary>
    /// Gets the app token
    /// </summary>
    /// <returns>The app token.</returns>
    public static string getAppToken()
    {
        if(InitSuccessful())
            return _usGetAppToken();

        return "UnityEditor-Token";
    }


    [DllImport("__Internal")]
    private static extern string _usGetPublicKey();

    /// <summary>
    /// Gets the public key
    /// </summary>
    /// <returns>The public key.</returns>
    public static string getPublicKey()
    {
        if(InitSuccessful())
            return _usGetPublicKey();

        return "UnityEditor-Key";
    }


    [DllImport("__Internal")]
    private static extern string _usGetSenderID();

    /// <summary>
    /// Gets the sender ID
    /// </summary>
    /// <returns>The SID.</returns>
    public static string getSid()
    {
        if(InitSuccessful())
            return _usGetSenderID();

        return "UnityEditor-Sid";
    }


    [DllImport("__Internal")]
    private static extern void _usSetLoggerLevel(int loggerLevel);

    /// <summary>
    /// Sets the logger leverl
    /// </summary>
    /// <param name="logLevel">Log level.</param>
    public static void setLoggerLevel(UpsightLoggerLevel loggerLevel)
    {
        if(InitSuccessful())
            _usSetLoggerLevel((int)loggerLevel);
    }


    [DllImport("__Internal")]
    private static extern string _usGetPluginVersion();

    /// <summary>
    /// Gets the plugin version
    /// </summary>
    /// <returns>The plugin version.</returns>
    public static string getPluginVersion()
    {
        if(InitSuccessful())
            return _usGetPluginVersion();

        return "UnityEditor";
    }


    [DllImport("__Internal")]
    private static extern bool _usGetOptOutStatus();

    /// <summary>
    /// Gets the opt out status.
    /// </summary>
    /// <returns>the opt out status</returns>
    public static bool getOptOutStatus()
    {
        if(InitSuccessful())
            return _usGetOptOutStatus();

        return false;
    }


    [DllImport("__Internal")]
    private static extern void _usSetLocation(double lat, double lon);

    /// <summary>
    /// Sets the user's location.
    /// </summary>
    /// <param name="lat">Lat.</param>
    /// <param name="lon">Lon.</param>
    public static void setLocation(double lat, double lon)
    {
        if(InitSuccessful())
            _usSetLocation(lat, lon);
    }


    /// <summary>
    /// Android only. Purges the stored location.
    /// </summary>
    public static void purgeLocation()
    { }



    [DllImport("__Internal")]
    private static extern void _usSetOptOutStatus(bool optOutStatus);

    /// <summary>
    /// Sets the opt out status.
    /// </summary>
    /// <param name="optOutStatus">If set to <c>true</c> opt out status.</param>
    public static void setOptOutStatus(bool optOutStatus)
    {
        if(InitSuccessful())
            _usSetOptOutStatus(optOutStatus);
    }

    #endregion

    private static bool InitSuccessful()
    {
        return Application.platform == RuntimePlatform.IPhonePlayer && Initialized;
    }
#endif
}
