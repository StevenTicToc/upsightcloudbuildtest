/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System.Collections.Generic;
using UpsightMiniJSON;
using DateTime = System.DateTime;

#if !UNITY_IOS
public partial class Upsight
{
    private static AndroidJavaObject _pluginBase;
    private static AndroidJavaObject _pluginPushExtension;
    private static AndroidJavaObject _pluginMarketingExtension;

    /// <summary>
    /// This instantiates the plugin classes. Should be called immediately at app launch.
    /// </summary>
    public static void init()
    {
        if(Application.platform != RuntimePlatform.Android)
            return;

        //Don't start the SDK if it's disabled
        try
        {
            using(var pluginClass = new AndroidJavaClass("com.upsight.android.unity.UpsightPlugin"))
            {
                if(!pluginClass.CallStatic<bool>("isEnabled"))
                {
                    Debug.LogWarning("Upsight.init() was called but the SDK is currently disabled from the Upsight SDK Management Extension. Upsight will not initialize.");
                    return;
                }
            }
        }
        catch
        {
            Debug.LogError("Error while checking if the Upsight SDK is enabled. Unable to initialize");
            return;
        }

        if(Initialized)
            return;

        Initialized = true;

        UpsightManager.init();

        try
        {
            _pluginBase = new AndroidJavaObject("com.upsight.android.unity.UpsightPlugin");

            if(_pluginBase == null || AndroidJNI.ExceptionOccurred() != System.IntPtr.Zero)
            {
                Debug.LogError("Upsight initialization failed! JNI Exception thrown:");
                AndroidJNI.ExceptionDescribe();

                _pluginBase = null;
                AndroidJNI.ExceptionClear();
                return;
            }
        }
        catch(System.Exception e)
        {
            Debug.LogError("Upsight initialization failed!");
            Debug.LogException(e);
            _pluginBase = null;
            return;
        }

        try
        {
            //IF MARKETING
            _pluginMarketingExtension = new AndroidJavaObject("com.upsight.android.unity.UpsightMarketingManager");
            if(_pluginMarketingExtension != null)
                _pluginBase.Call("registerExtension", _pluginMarketingExtension);
        }
        catch
        {
            Debug.LogWarning("Upsight Marketing Extension not included.");
            _pluginMarketingExtension = null;
        }

        try
        {
            //IF PUSH
            _pluginPushExtension = new AndroidJavaObject("com.upsight.android.unity.UpsightPushManager");
            if(_pluginPushExtension != null)
                _pluginBase.Call("registerExtension", _pluginPushExtension);
        }
        catch
        {
            Debug.LogWarning("Upsight Push Extension not included.");
            _pluginPushExtension = null;
        }
    }

    /// <summary>
    /// This removes all references to the plugin classes (android bridge) and resets the init state.
    /// This should only be called when disabling the SDK in order to remove the old UpsightPlugin class.
    /// </summary>
    internal static void terminate()
    {
        if(_pluginBase != null)
            _pluginBase.Dispose();

        if(_pluginMarketingExtension != null)
            _pluginMarketingExtension.Dispose();

        if(_pluginPushExtension != null)
            _pluginPushExtension.Dispose();

        _pluginBase = null;
        _pluginMarketingExtension = null;
        _pluginPushExtension = null;
        Initialized = false;
    }


    #region User Attributes

    /// <summary>
    /// Starts a new user session for a specified userID.
    /// </summary>
    /// <remarks>
    /// userID cannot be the empty string and has a max length of 256 bytes when encoded in UTF-8.
    /// The user remain "logged in" between app starts.
    /// You cannot explicitly log into the anonymous user.
    /// If there is no userID currently set, the user is tracked as an anonymous user.
    /// The anonymous user is treated as a regular user with data persisting between sessions.
    /// User Attributes are associated with a userID and will change when a different userID is set.
    /// Device sessions are independant from user sessions. Starting a new user session does not start a new device session.
    /// SWITCHING BETWEEN USERS:
    /// Logging into Existing User: Previous user info is saved and the user is "logged out" (including anonymous users). Existing user is "logged in".
    /// Existing User -> Anonymous User: Previous user info in saved and user is "logged out". Anonymous user is "logged in".
    /// Existing User -> New User: Previous user info in saved and user is "logged out". A new user is created and "logged in" using the default values.
    /// Anonymous User -> New User: The 'preserve' parameter determines whether the anonymous user data should be copied into the new user.
    /// If 'preserve' is false, the anonymous user is "logged out", a new user is created using default values and "logged in".
    /// If 'preserve' is true, the anonymous user data is copied over to to the newly created user, the new user is "logged in", and the anonymous user data is reset to default.
    /// </remarks>
    /// <param name="id">The id of the user that should be "logged in".</param>
    /// <param name="preserve">Determines the behaviour when switching from an anonymous user to a new user.</param>
    /// <returns>true if successful; false otherwise</returns>
    public static UpsightUserResult setUserID(string id, bool preserve)
    {
        if(!initSuccessful())
            return UpsightUserResult.NativeNotInitialized;

        return (UpsightUserResult) _pluginBase.Call<int>("setUserID", id, preserve);
    }


    /// <summary>
    /// Ends a user session for the currently "logged in" user. The anonymous user is "logged in".
    /// </summary>
    public static UpsightUserResult clearUserID()
    {
        if(!initSuccessful())
            return UpsightUserResult.NativeNotInitialized;

        return (UpsightUserResult)_pluginBase.Call<int>("clearUserID");
    }


    /// <summary>
    /// Removes all data stored for the specified userID. You cannot delete the anonymous user.
    /// </summary>
    /// <param name="id">The id of the user to delete.</param>
    public static UpsightUserResult deleteUser(string id)
    {
        if(!initSuccessful())
            return UpsightUserResult.NativeNotInitialized;

        return (UpsightUserResult)_pluginBase.Call<int>("deleteUser", id);
    }


    /// <summary>
    /// Returns the id of the currently "logged in" user.
    /// </summary>
    /// <returns>The current userID.</returns>
    public static string getCurrentUserID()
    {
        if(!initSuccessful())
            return null;

        return _pluginBase.Call<string>("getCurrentUserID");
    }


    /// <summary>
    /// Returns the session number of the currently "logged in" user.
    /// </summary>
    /// <returns>The session number for the current user.</returns>
    public static int getCurrentUserSessionNumber()
    {
        if(!initSuccessful())
            return -1;

        return _pluginBase.Call<int>("getCurrentUserSessionNumber");
    }


    /// <summary>
    /// Returns the start time of the session for the currently "logged in" user.
    /// </summary>
    /// <returns>The start time of the current user session.</returns>
    public static DateTime getCurrentUserSessionStartTime()
    {
        if(!initSuccessful())
            return default(DateTime);

        return _pluginBase.Call<long>("getCurrentUserSessionStartTime").ToDateTime();
    }


    /// <summary>
    /// Resets user attributes to the default values for the currently "logged in" user.
    /// </summary>
    public static void resetUserAttributes()
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("resetUserAttributes");
    }


    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeString(string key, string value)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("setUserAttributesString", key, value);
    }


    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeFloat(string key, float value)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("setUserAttributesFloat", key, value);
    }


    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeInt(string key, int value)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("setUserAttributesInt", key, value);
    }


    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeBool(string key, bool value)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("setUserAttributesBool", key, value);
    }


    /// <summary>
    /// Sets the user attribute
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="value">Value.</param>
    public static void setUserAttributeDate(string key, DateTime value)
    {
        if(!initSuccessful())
            return;

        long lSeconds = value.ToUnixTimestamp();
        _pluginBase.Call("setUserAttributesDatetime", key, lSeconds);
    }


    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes string.</returns>
    /// <param name="key">Key.</param>
    public static string getUserAttributeString(string key)
    {
        if(!initSuccessful())
            return null;

        return _pluginBase.Call<string>("getUserAttributesString", key);
    }


    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes float.</returns>
    /// <param name="key">Key.</param>
    public static float getUserAttributeFloat(string key)
    {
        if(!initSuccessful())
            return 0f;

        return _pluginBase.Call<float>("getUserAttributesFloat", key);
    }


    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes int.</returns>
    /// <param name="key">Key.</param>
    public static int getUserAttributeInt(string key)
    {
        if(!initSuccessful())
            return 0;

        return _pluginBase.Call<int>("getUserAttributesInt", key);
    }


    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes bool.</returns>
    /// <param name="key">Key.</param>
    public static bool getUserAttributeBool(string key)
    {
        if(!initSuccessful())
            return false;

        return _pluginBase.Call<bool>("getUserAttributesBool", key);
    }


    /// <summary>
    /// Gets the user attribute
    /// </summary>
    /// <returns>The user attributes date.</returns>
    /// <param name="key">Key.</param>
    public static DateTime getUserAttributeDate(string key)
    {
        if(!initSuccessful())
            return default(DateTime);

        return _pluginBase.Call<long>("getUserAttributesDatetime", key).ToDateTime();
    }

    #endregion

    #region GDPR

    /// <summary>
    /// Sets the user's current country
    /// </summary>
    /// <param name="countryCode">Country Code.</param>
    public static bool setCurrentCountry(string country)
    {
        if (!initSuccessful())
            return false;

        return _pluginBase.Call<bool>("setCurrentCountry", country);
    }

    /// <summary>
    /// Gets the user's current country
    /// </summary>
    /// <returns>The user current country string.</returns>
    public static string getCurrentCountry()
    {
        if(!initSuccessful())
            return null;

        return _pluginBase.Call<string>("getCurrentCountry");
    }

    /// <summary>
    /// Sets the user's home country
    /// </summary>
    /// <param name="countryCode">Country Code.</param>
    public static bool setHomeCountry(string country)
    {
        if (!initSuccessful())
            return false;

        return _pluginBase.Call<bool>("setHomeCountry", country);
    }

    /// <summary>
    /// Gets the user's home country
    /// </summary>
    /// <returns>The user home country string.</returns>
    public static string getHomeCountry()
    {
        if(!initSuccessful())
            return null;

        return _pluginBase.Call<string>("getHomeCountry");
    }

    /// <summary>
    /// Sets the user's enhanced privacy applicability
    /// </summary>
    /// <param name="level">True if enhance privacy applies to the user. False if it does not. Null to let the Upsight platform decide</param>
    public static void setEnhancedPrivacyApplicability(UpsightEnhancedPrivacyApplicability level)
    {
        if (!initSuccessful())
            return;

        _pluginBase.Call("setEnhancedPrivacyApplicability", (int)level);
    }

    /// <summary>
    /// Gets user's enhanced privacy applicability level
    /// </summary>
    /// <returns>The user's enhanced privacy applicability level.</returns>
    public static UpsightEnhancedPrivacyApplicability getEnhancedPrivacyApplicability()
    {
        if(!initSuccessful())
            return UpsightEnhancedPrivacyApplicability.Auto;

        return (UpsightEnhancedPrivacyApplicability)_pluginBase.Call<int>("getEnhancedPrivacyApplicability");
    }

    /// <summary>
    /// Sets the user's enhanced privacy consent
    /// </summary>
    /// <param name="granted">Consent granted.</param>
    public static void setEnhancedPrivacyConsentGranted(bool granted)
    {
        if (!initSuccessful())
            return;

        _pluginBase.Call("setEnhancedPrivacyConsentGranted", granted);
    }

    /// <summary>
    /// Gets whether the user has granted consent for enhanced privacy
    /// </summary>
    /// <returns>The user's enhanced privacy consent state.</returns>
    public static bool isEnhancedPrivacyConsentGranted()
    {
        if(!initSuccessful())
            return false;

        return _pluginBase.Call<bool>("isEnhancedPrivacyConsentGranted");
    }

    #endregion


    #region Events and Milestones

    /// <summary>
    /// Reports a custom event not associated with a particular session
    /// </summary>
    /// <param name="eventName">Custom event name.</param>
    /// <param name="properties">Properties.</param>
    public static void recordSessionlessCustomEvent(string eventName, Dictionary<string, object> properties = null)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("recordSessionlessAnalyticsEvent", eventName, properties != null ? Json.Serialize(properties) : null);
	} 
	/// <summary>
	/// Reports a custom event not associated with a particular session
	/// </summary>
	/// <param name="eventName">Custom event name.</param>
	/// <param name="Value">Value of the event.</param>
	/// <param name="properties">Properties.</param>
	public static void recordSessionlessCustomEvent(string eventName,int value, Dictionary<string, object> properties = null)
	{
		if(!initSuccessful())
			return;

		_pluginBase.Call("recordSessionlessAnalyticsEvent", eventName, value, properties != null ? Json.Serialize(properties) : null);
	}
	public static void recordSessionlessCustomEvent(string eventName,double value, Dictionary<string, object> properties = null)
	{
		if(!initSuccessful())
			return;

		_pluginBase.Call("recordSessionlessAnalyticsEvent", eventName, value, properties != null ? Json.Serialize(properties) : null);
	}


    /// <summary>
    /// Reports a custom event
    /// </summary>
    /// <param name="eventName">Custom event name.</param>
    /// <param name="properties">Properties.</param>
    public static void recordCustomEvent(string eventName, Dictionary<string, object> properties = null)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("recordAnalyticsEvent", eventName, properties != null ? Json.Serialize(properties) : null);
	}
	/// <summary>
	/// Reports a custom event
	/// </summary>
	/// <param name="eventName">Custom event name.</param>
	/// <param name="Value">Value of the event.</param>
	/// <param name="properties">Properties.</param>
	public static void recordCustomEvent(string eventName,int value, Dictionary<string, object> properties = null)
	{
		if(!initSuccessful())
			return;

		_pluginBase.Call("recordAnalyticsEvent", eventName, value, properties != null ? Json.Serialize(properties) : null);
	}

	public static void recordCustomEvent(string eventName,double value, Dictionary<string, object> properties = null)
	{
		if(!initSuccessful())
			return;

		_pluginBase.Call("recordAnalyticsEvent", eventName, value, properties != null ? Json.Serialize(properties) : null);
	}


    /// <summary>
    /// Reports a milestone event for the given scope
    /// </summary>
    /// <param name="scope">Scope.</param>
    public static void recordMilestoneEvent(string scope, Dictionary<string, object> properties = null)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("recordMilestoneEvent", scope, properties != null ? Json.Serialize(properties) : null);
    }


    /// <summary>
    /// Indicates if there is content available for the billboard with a given scope
    /// </summary>
    /// <param name="scope">Scope.</param>
    public static bool isContentReadyForBillboardWithScope(string scope)
    {
        if(!initSuccessful() || _pluginMarketingExtension == null)
            return false;

        return _pluginMarketingExtension.Call<bool>("isContentReadyForBillboardWithScope", scope);
    }


    /// <summary>
    /// Prepares a billboard for display
    /// </summary>
    /// <param name="scope">Scope.</param>
    public static void prepareBillboard(string scope)
    {
        if(!initSuccessful() || _pluginMarketingExtension == null)
            return;

        _pluginMarketingExtension.Call("prepareBillboard", scope);
    }


    /// <summary>
    /// Destroys the loaded billboard for the given scope
    /// </summary>
    public static void destroyBillboard(string scope)
    {
        if(!initSuccessful() || _pluginMarketingExtension == null)
            return;

        _pluginMarketingExtension.Call("destroyBillboard", scope);
    }


    /// <summary>
    /// Records a monetization event
    /// </summary>
    /// <param name="totalPrice">Total price.</param>
    /// <param name="currency">Currency.</param>
    /// <param name="resolution">Resolution.</param>
    /// <param name="product">Product.</param>
    /// <param name="price">Price.</param>
    /// <param name="quantity">Quantity.</param>
    /// <param name="properties">Properties.</param>
    public static void recordMonetizationEvent(double totalPrice, string currency, UpsightPurchaseResolution resolution, string product = null, double price = -1f, int quantity = -1, Dictionary<string, object> properties = null)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("recordMonetizationEvent", totalPrice, currency, product, price, resolution.ToString().ToLower(), quantity, properties != null ? Json.Serialize(properties) : null);
    }


    /// <summary>
    /// Android only. Records a purchase.
    /// </summary>
    /// <param name="quantity">Quantity.</param>
    /// <param name="currency">Currency.</param>
    /// <param name="price">Price.</param>
    /// <param name="totalPrice">Total price.</param>
    /// <param name="product">Product.</param>
    /// <param name="responseCode">Response code.</param>
    /// <param name="inAppPurchaseData">In-app purchase data.</param>
    /// <param name="inAppDataSignature">In-app data signature.</param>
    /// <param name="properties">Properties.</param>
    public static void recordGooglePlayPurchase(int quantity, string currency, double price, double totalPrice, string product, int responseCode, string inAppPurchaseData, string inAppDataSignature, Dictionary<string, object> properties = null)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("recordGooglePlayPurchase", quantity, currency, price, totalPrice, product, responseCode, inAppPurchaseData, inAppDataSignature, properties != null ? Json.Serialize(properties) : null);
    }


    /// <summary>
    /// iOS only. Records the Apple store purchase
    /// </summary>
    /// <param name="quantity">Quantity.</param>
    /// <param name="currency">Currency.</param>
    /// <param name="price">Price.</param>
    /// <param name="transactionIdentifier">Transaction identifier.</param>
    /// <param name="product">Product.</param>
    /// <param name="resolution">Resolution.</param>
    /// <param name="properties">Properties.</param>
    public static void recordAppleStorePurchase(int quantity, string currency, double price, string transactionIdentifier, string product, UpsightPurchaseResolution resolution, Dictionary<string, object> properties = null)
    { }


    /// <summary>
    /// An event that notifies the server of the attribution source for a new install.
    /// </summary>
    /// <param name="campaign">The campaign ID for this creative.</param>
    /// <param name="creative">The ID for the creative receiving attribution.</param>
    /// <param name="source">The network source for the creative.</param>
    /// <param name="properties">A dictionary containing any information you would like to associate with this event.</param>
    public static void recordAttributionEvent(string campaign, string creative, string source, Dictionary<string, object> properties = null)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("recordAttributionEvent", campaign, creative, source, properties != null ? Json.Serialize(properties) : null);
    }

    #endregion


    #region Push Notifications

    /// <summary>
    /// Registers the current device for push notifications.
    /// </summary>
    public static void registerForPushNotifications()
    {
        if(!initSuccessful() || _pluginPushExtension == null)
            return;

        _pluginPushExtension.Call("registerForPushNotifications");
    }


    /// <summary>
    /// Unregisters the current device for push notifications.
    /// </summary>
    public static void unregisterForPushNotifications()
    {
        if(!initSuccessful() || _pluginPushExtension == null)
            return;

        _pluginPushExtension.Call("unregisterForPushNotifications");
    }

    #endregion


    #region UXM

    /// <summary>
    /// Sets a boolean flag indicating if the SDK should apply Managed Variable update.
    /// </summary>
    /// <param name="shouldSynchronize">Set to true to apply new user experience bundles; or false to skip.</param>
    public static void setShouldSynchronizeManagedVariables(bool shouldSynchronize)
    {
        using(var pluginClass = new AndroidJavaClass("com.upsight.android.unity.UnitySessionCallbacks"))
        {
            pluginClass.CallStatic("setShouldSynchronizeManagedVariables", shouldSynchronize);
        }
    }


    /// <summary>
    /// Gets the managed variable
    /// </summary>
    /// <returns>The managed string.</returns>
    /// <param name="key">Key.</param>
    public static string getManagedString(string key)
    {
        if(!initSuccessful())
            return null;

        return _pluginBase.Call<string>("getManagedString", key);
    }


    /// <summary>
    /// Gets the managed variable
    /// </summary>
    /// <returns>The managed float.</returns>
    /// <param name="key">Key.</param>
    public static float getManagedFloat(string key)
    {
        if(!initSuccessful())
            return 0f;

        return _pluginBase.Call<float>("getManagedFloat", key);
    }


    /// <summary>
    /// Gets the managed variable
    /// </summary>
    /// <returns>The managed int.</returns>
    /// <param name="key">Key.</param>
    public static int getManagedInt(string key)
    {
        if(!initSuccessful())
            return 0;

        return _pluginBase.Call<int>("getManagedInt", key);
    }


    /// <summary>
    /// Gets the managed variable
    /// </summary>
    /// <returns>The managed bool.</returns>
    /// <param name="key">Key.</param>
    public static bool getManagedBool(string key)
    {
        if(!initSuccessful())
            return false;

        return _pluginBase.Call<bool>("getManagedBool", key);
    }

    #endregion


    #region Miscellaneous

    /// <summary>
    /// Gets the app token
    /// </summary>
    /// <returns>The app token.</returns>
    public static string getAppToken()
    {
        if(Application.platform != RuntimePlatform.Android)
            return "UnityEditor-Token";
        else if(_pluginBase == null || !Initialized)
            return null;

        return _pluginBase.Call<string>("getAppToken");
    }


    /// <summary>
    /// Gets the public key
    /// </summary>
    /// <returns>The public key.</returns>
    public static string getPublicKey()
    {
        if(Application.platform != RuntimePlatform.Android)
            return "UnityEditor-Key";
        else if(_pluginBase == null || !Initialized)
            return null;

        return _pluginBase.Call<string>("getPublicKey");
    }


    /// <summary>
    /// Gets the SID
    /// </summary>
    /// <returns>The SID.</returns>
    public static string getSid()
    {
        if(!initSuccessful())
            return null;

        return _pluginBase.Call<string>("getSid");
    }


    /// <summary>
    /// Sets the logger leverl
    /// </summary>
    /// <param name="logLevel">Log level.</param>
    public static void setLoggerLevel(UpsightLoggerLevel loggerLevel)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("setLoggerLevel", loggerLevel.ToString().ToUpper());
    }


    /// <summary>
    /// Gets the plugin version
    /// </summary>
    /// <returns>The plugin version.</returns>
    public static string getPluginVersion()
    {
        if(Application.platform != RuntimePlatform.Android)
            return "UnityEditor";
        else if(_pluginBase == null || !Initialized)
            return null;

        return _pluginBase.Call<string>("getPluginVersion");
    }


    /// <summary>
    /// Gets the opt out status.
    /// </summary>
    /// <returns>the opt out status</returns>
    public static bool getOptOutStatus()
    {
        if(!initSuccessful())
            return false;

        return _pluginBase.Call<bool>("getOptOutStatus");
    }


    /// <summary>
    /// Sets the opt out status.
    /// </summary>
    /// <param name="optOutStatus">If set to <c>true</c> opt out status.</param>
    public static void setOptOutStatus(bool optOutStatus)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("setOptOutStatus", optOutStatus);
    }


    /// <summary>
    /// Sets the user's location.
    /// </summary>
    /// <param name="lat">Lat.</param>
    /// <param name="lon">Lon.</param>
    public static void setLocation(double lat, double lon)
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("setLocation", lat, lon);
    }


    /// <summary>
    /// Android only. Purges the stored location.
    /// </summary>
    public static void purgeLocation()
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("purgeLocation");
    }

    /// <summary>
    /// Gets session number of the latest Upsight session.
    /// </summary>
    /// <returns>The session number.</returns>
    public static int getLatestSessionNumber()
    {
        if(!initSuccessful())
            return 0;

        return _pluginBase.Call<int>("getLatestSessionNumber");
    }

    /// <summary>
    /// Gets session start timestamp of the latest Upsight session.
    /// </summary>
    /// <returns>The session start timestamp.</returns>
    public static long getLatestSessionStartTimestamp()
    {
        if(!initSuccessful())
            return 0L;

        return _pluginBase.Call<long>("getLatestSessionStartTimestamp");
    }

    #endregion


    public static void onPause()
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("onApplicationPaused");
    }

    public static void onResume()
    {
        if(!initSuccessful())
            return;

        _pluginBase.Call("onApplicationResumed");
    }

    private static bool initSuccessful()
    {
        return Application.platform == RuntimePlatform.Android && Initialized && _pluginBase != null;
    }
}
#endif
