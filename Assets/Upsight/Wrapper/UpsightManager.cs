/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System;
using System.Collections.Generic;
using UpsightMiniJSON;


public class UpsightManager : MonoBehaviour
{
    #region Constructor and Lifecycle

    public const string GameObjectName = "UpsightManager";
    private static bool initialized = false;
    private bool _destroyed = false;

    public static void init()
    {
        if(initialized)
            return;

        initialized = true;

        //Create a GameObject named <GameObjectName> and attach a UpsightManager component to it if they don't exist
        var upsightGameObject = GameObject.Find(GameObjectName) ?? new GameObject(GameObjectName);
        var managers = upsightGameObject.GetComponent<UpsightManager>() ?? upsightGameObject.AddComponent<UpsightManager>();
        //Will never happen, but makes an unused warning go away
        if(managers == null)
            Debug.LogError("No UpsightManager Component on GameObject");

        DontDestroyOnLoad(upsightGameObject);

        Upsight.init();
    }

    private void Awake()
    {
        if(_destroyed)
            return;

        //Look for all UpsightManager Components
        var managers = FindObjectsOfType<UpsightManager>();
        bool foundOnUpsightGo = false;
        if(managers.Length > 1)
        {
            foreach(var m in managers)
            {
                //When we find the first one on the Upsight GameObject leave it alone. Delete all the rest (including ourselves)
                if(m.gameObject.name == GameObjectName && !foundOnUpsightGo)
                {
                    foundOnUpsightGo = true;
                }
                else if(!m._destroyed)
                {
                    m._destroyed = true;
                    Destroy(m);
                }
            }
        }

        //If we didn't find one on the Upsigth GameObject create it now
        if(!foundOnUpsightGo)
            init();
        else
            initialized = true;
    }

    #endregion


    /// <summary>
    /// Fired when an Upsight session is started
    /// </summary>
    public static event Action sessionDidStartEvent;

    /// <summary>
    /// Fired when an Upsight session is resumed
    /// </summary>
    public static event Action sessionDidResumeEvent;

    /// <summary>
    /// Fired when an Upsight user session is started
    /// </summary>
    public static event Action userSessionDidStartEvent;

    /// <summary>
    /// Fired when an Upsight user session is resumed
    /// </summary>
    public static event Action userSessionDidResumeEvent;

    /// <summary>
    /// Fired when managed variables are synchronized with values stored on the Upsight server
    /// </summary>
    public static event Action<List<string>> managedVariablesDidSynchronizeEvent;

    /// <summary>
    /// Fired when a billboard is shown
    /// </summary>
    public static event Action<string, UpsightContentAttributes> onBillboardAppearEvent;

    /// <summary>
    /// Fired when a billboard is dismissed
    /// </summary>
    public static event Action<string> onBillboardDismissEvent;

    /// <summary>
    /// Fired when a reward is unlocked
    /// </summary>
    public static event Action<UpsightReward> billboardDidReceiveRewardEvent;

    /// <summary>
    /// Fired when a purchase is unlocked
    /// </summary>
    public static event Action<UpsightPurchase> billboardDidReceivePurchaseEvent;

    /// <summary>
    /// Fired when a data payload is received
    /// </summary>
    public static event Action<UpsightData> billboardDidReceiveDataEvent;

    /// <summary>
    /// Fired when content becomes ready for a scope
    /// </summary>
    public static event Action<string, Dictionary<string, string>> onContentAvailableEvent;

    /// <summary>
    /// Fired when content is not available for a scope
    /// </summary>
    public static event Action<string, Dictionary<string, string>> onContentNotAvailableEvent;

    /// <summary>
    /// Fired when a marketing provider has finished loading
    /// </summary>
    public static event Action<string> onPartnerInitializedEvent;


    #region Private methods called from native code

    void sessionDidStart()
    {
        if(sessionDidStartEvent != null)
            sessionDidStartEvent();
    }

    void sessionDidResume()
    {
        if(sessionDidResumeEvent != null)
            sessionDidResumeEvent();
    }

    void userSessionDidStart()
    {
        if(userSessionDidStartEvent != null)
            userSessionDidStartEvent();
    }

    void userSessionDidResume()
    {
        if(userSessionDidResumeEvent != null)
            userSessionDidResumeEvent();
    }

    void managedVariablesDidSynchronize(string json)
    {
        if(managedVariablesDidSynchronizeEvent != null)
        {
            List<string> tags = null;

            if(null != json && 0 < json.Length)
            {
                var array = Json.ToJsonArray(json);
                tags = new List<string>();
                foreach(var tag in array)
                    tags.Add(tag.ToString());
            }

            managedVariablesDidSynchronizeEvent(tags);
        }
    }

    void onBillboardAppear(string json)
    {
        string scope;
        var content = UpsightContentAttributes.FromJson(json, out scope);
        if(onBillboardAppearEvent != null)
            onBillboardAppearEvent(scope, content);
    }


    void onBillboardDismiss(string scope)
    {
        if(onBillboardDismissEvent != null)
            onBillboardDismissEvent(scope);
    }


    void billboardDidReceiveReward(string json)
    {
        if(billboardDidReceiveRewardEvent != null)
            billboardDidReceiveRewardEvent(UpsightReward.rewardFromJson(json));
    }


    void billboardDidReceivePurchase(string json)
    {
        if(billboardDidReceivePurchaseEvent != null)
            billboardDidReceivePurchaseEvent(UpsightPurchase.purchaseFromJson(json));
    }

    void billboardDidReceiveData(string json)
    {
        if(billboardDidReceiveDataEvent != null)
            billboardDidReceiveDataEvent(UpsightData.FromJson(json));
    }

    void onContentAvailable(string json)
    {
        string scope;
        Dictionary<string, string> data;
        if(onContentAvailableEvent != null && parseContentJson(json, out scope, out data))
            onContentAvailableEvent(scope, data);
    }

    void onContentNotAvailable(string json)
    {
        string scope;
        Dictionary<string, string> data;
        if(onContentNotAvailableEvent != null && parseContentJson(json, out scope, out data))
            onContentNotAvailableEvent(scope, data);
    }

    void onPartnerInitialized(string partnerName)
    {
        if(onPartnerInitializedEvent != null)
            onPartnerInitializedEvent(partnerName);
    }

    #endregion

    private bool parseContentJson(string json, out string scope, out Dictionary<string, string> data)
    {
        scope = null;
        data = null;

        var dict = Json.ToJsonObject(json);
        if(dict != null)
        {
            if(dict.ContainsKey("scope"))
                scope = dict["scope"].ToString();
            else
                Debug.LogWarning("No 'scope' while parsing content callback json");

            if(dict.ContainsKey("data"))
                data = dict.GetPrimitiveDictionary<string>("data");
            else
                Debug.LogWarning("No 'data' while parsing content callback json");
        }

        return scope != null && data != null;
    }


#if UNITY_ANDROID
    void OnApplicationPause(bool paused)
    {
        if(paused)
            Upsight.onPause();
        else
            Upsight.onResume();
    }
#endif

}
