/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System.Collections.Generic;
using UpsightMiniJSON;

public class UpsightPurchase
{
    public string productIdentifier { get; private set; }
    public int quantity { get; private set; }
    public string billboardScope { get; private set; }

    public static UpsightPurchase purchaseFromJson(string json)
    {
        var purchase = new UpsightPurchase();
        purchase.populateFromJson(json);
        return purchase;
    }


    protected void populateFromJson(string json)
    {
        var dict = Json.ToJsonObject(json);
        if(dict != null)
        {
            if(dict.ContainsKey("productIdentifier"))
                productIdentifier = dict["productIdentifier"].ToString();
            else
                Debug.LogWarning("No 'productIdentifier' while parsing UpsightPurchase json");

            if(dict.ContainsKey("quantity"))
                quantity = dict.GetPrimitive<int>("quantity");
            else
                Debug.LogWarning("No 'quantity' while parsing UpsightPurchase json");

            if(dict.ContainsKey("billboardScope"))
                billboardScope = dict["billboardScope"].ToString();
            else
                Debug.LogWarning("No 'billboardScope' while parsing UpsightPurchase json");
        }
    }

    public override string ToString()
    {
        return string.Format("[UpsightPurchase] productIdentifier: {0}, quantity: {1}, billboardScope: {2}", productIdentifier, quantity, billboardScope);
    }
}
