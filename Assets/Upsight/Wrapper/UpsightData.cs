/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Globalization;
using UpsightMiniJSON;

public partial class UpsightData
{
#if UNITY_ANDROID
    private AndroidJavaClass _handlerClass;

    public UpsightData()
    {
        _handlerClass = new AndroidJavaClass("com.upsight.android.unity.BillboardHandler");
    }

    ~UpsightData()
    {
        _handlerClass.Dispose();
    }
#endif

#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern bool _usUpsightDataRecord(string dispatchName);
    [DllImport("__Internal")]
    private static extern void _usUpsightDataDestroy();
    [DllImport("__Internal")]
    private static extern void _usUpsightDataRecordImpressionEvent();
    [DllImport("__Internal")]
    private static extern void _usUpsightDataRecordClickEvent();
    [DllImport("__Internal")]
    private static extern void _usUpsightDataRecordDismissEvent();
#endif

    private Dictionary<string, string> _stringProperties = new Dictionary<string, string>();
    private Dictionary<string, bool> _boolProperties = new Dictionary<string, bool>();
    private Dictionary<string, int> _intProperties = new Dictionary<string, int>();
    private Dictionary<string, float> _floatProperties = new Dictionary<string, float>();
    private Dictionary<string, Image> _imageProperties = new Dictionary<string, Image>();
    private Dictionary<string, Color> _colorProperties = new Dictionary<string, Color>();
    private string _rawData;

    public string GetString(string key)
    {
        if(_stringProperties.ContainsKey(key))
            return _stringProperties[key];
        else
            return null;
    }

    public bool GetBool(string key)
    {
        if(_boolProperties.ContainsKey(key))
            return _boolProperties[key];
        else
            return false;
    }

    public int GetInt(string key)
    {
        if(_intProperties.ContainsKey(key))
            return _intProperties[key];
        else
            return 0;
    }

    public float GetFloat(string key)
    {
        if(_floatProperties.ContainsKey(key))
            return _floatProperties[key];
        else
            return 0f;
    }

    public Image GetImage(string key)
    {
        if(_imageProperties.ContainsKey(key))
            return _imageProperties[key];
        else
            return default(Image);
    }

    public Color GetColor(string key)
    {
        if(_colorProperties.ContainsKey(key))
            return _colorProperties[key];
        else
            return default(Color);
    }

    public string GetRawData()
    {
        return _rawData;
    }

    public bool Record(string eventName)
    {
#if UNITY_ANDROID
        try
        {
            return _handlerClass.CallStatic<bool>("record", eventName);
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling com.upsight.android.unity.BillboardHandler.record");
            Debug.LogException(e);
            return false;
        }
#elif UNITY_IOS
        try
        {
            return _usUpsightDataRecord(eventName);
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling _usUpsightDataRecord");
            Debug.LogException(e);
            return false;
        }
#else
        return false;
#endif
    }

    public void Destroy()
    {
#if UNITY_ANDROID
        try
        {
            _handlerClass.CallStatic("destroy");
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling com.upsight.android.unity.BillboardHandler.destroy");
            Debug.LogException(e);
        }
#elif UNITY_IOS
        try
        {
            _usUpsightDataDestroy();
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling _usUpsightDataDestroy");
            Debug.LogException(e);
        }
#endif
    }

    public void RecordImpressionEvent()
    {
#if UNITY_ANDROID
        try
        {
            _handlerClass.CallStatic("recordImpressionEvent");
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling com.upsight.android.unity.BillboardHandler.recordImpressionEvent");
            Debug.LogException(e);
        }
#elif UNITY_IOS
        try
        {
            _usUpsightDataRecordImpressionEvent();
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling _usUpsightDataRecordImpressionEvent");
            Debug.LogException(e);
        }
#endif
    }

    public void RecordClickEvent()
    {
#if UNITY_ANDROID
        try
        {
            _handlerClass.CallStatic("recordClickEvent");
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling com.upsight.android.unity.BillboardHandler.recordClickEvent");
            Debug.LogException(e);
        }
#elif UNITY_IOS
        try
        {
            _usUpsightDataRecordClickEvent();
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling _usUpsightDataRecordClickEvent");
            Debug.LogException(e);
        }
#endif
    }

    public void RecordDismissEvent()
    {
#if UNITY_ANDROID
        try
        {
            _handlerClass.CallStatic("recordDismissEvent");
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling com.upsight.android.unity.BillboardHandler.recordDismissEvent");
            Debug.LogException(e);
        }
#elif UNITY_IOS
        try
        {
            _usUpsightDataRecordDismissEvent();
        }
        catch(System.Exception e)
        {
            Debug.LogError("Error while calling _usUpsightDataRecordDismissEvent");
            Debug.LogException(e);
        }
#endif
    }

    public static UpsightData FromJson(string json)
    {
        var data = new UpsightData();
        data.populateFromJson(json);
        return data;
    }

    protected void populateFromJson(string json)
    {
        try
        {
            var dict = Json.ToJsonObject(json);
            if(dict != null)
            {
                _stringProperties = dict.GetPrimitiveDictionary<string>("string");
                _boolProperties = dict.GetPrimitiveDictionary<bool>("bool");
                _intProperties = dict.GetPrimitiveDictionary<int>("int");
                _floatProperties = dict.GetPrimitiveDictionary<float>("float");

                var images = dict.GetJsonObject("image");
                if(images != null)
                {
                    _imageProperties = new Dictionary<string, Image>();
                    foreach(string imageKey in images.Keys)
                    {
                        var imageDict = images.GetJsonObject(imageKey);
                        string path;
                        int width;
                        int height;

                        if(imageDict.TryGetPrimitive("image", out path) && imageDict.TryGetPrimitive("width", out width) && imageDict.TryGetPrimitive("height", out height))
                            _imageProperties.Add(imageKey, new Image() { ImagePath = path, Width = width, Height = height });
                    }
                }

                var colors = dict.GetPrimitiveDictionary<string>("color");
                if(colors != null)
                {
                    //Expected format (#RGBA) #FFFFFFFF
                    _colorProperties = new Dictionary<string, Color>();

                    foreach(var e in colors)
                    {
                        uint colorBits;
                        if(uint.TryParse(e.Value.Substring(1), NumberStyles.AllowHexSpecifier, NumberFormatInfo.CurrentInfo, out colorBits))
                        {
                            Color color = new Color32((byte)(colorBits >> 24), (byte)(colorBits >> 16), (byte)(colorBits >> 8), (byte)(colorBits));
                            _colorProperties.Add(e.Key, color);
                        }
                        else
                        {
                            Debug.LogError("Error while parsing color hex value. Unable to parse " + e.Value + " as a hex string");
                        }
                    }
                }

                if(dict.ContainsKey("raw") && dict["raw"] != null)
                    _rawData = Json.Serialize(dict["raw"]);
            }
        }
        catch
        {
            Debug.LogError("Unable to parse UpsightData: " + json);
        }
    }

    public struct Image
    {
        public string ImagePath;
        public int Width;
        public int Height;

        public override string ToString()
        {
            return string.Format("{{ \"UpsightData.Image\" : {{ \"ImagePath\": \"{0}\", \"Width\": {1}, \"Height\": {2} }} }}", ImagePath, Width, Height);
        }
    }
}
