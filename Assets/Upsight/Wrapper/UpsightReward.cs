/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System.Collections.Generic;
using UpsightMiniJSON;

public class UpsightReward
{
    public string productIdentifier { get; private set; }
    public int quantity { get; private set; }
    public string signatureData { get; private set; }
    public string billboardScope { get; private set; }


    public static UpsightReward rewardFromJson(string json)
    {
        var reward = new UpsightReward();
        reward.populateFromJson(json);
        return reward;
    }


    protected void populateFromJson(string json)
    {
        var dict = Json.ToJsonObject(json);
        if(dict != null)
        {
            if(dict.ContainsKey("productIdentifier"))
                productIdentifier = dict["productIdentifier"].ToString();
            else
                Debug.LogWarning("No 'productIdentifier' while parsing UpsightReward json");

            if(dict.ContainsKey("quantity"))
                quantity = int.Parse(dict["quantity"].ToString());
            else
                Debug.LogWarning("No 'quantity' while parsing UpsightReward json");

            if(dict.ContainsKey("signatureData"))
                signatureData = Json.Serialize(dict["signatureData"]);
            else
                Debug.LogWarning("No 'signatureData' while parsing UpsightReward json");

            if(dict.ContainsKey("billboardScope"))
                billboardScope = dict["billboardScope"].ToString();
            else
                Debug.LogWarning("No 'billboardScope' while parsing UpsightReward json");
        }
    }


    public override string ToString()
    {
        return string.Format("[UpsightReward] productIdentifier: {0}, quantity: {1}, signatureData: {2}, billboardScope: {3}", productIdentifier, quantity, signatureData, billboardScope);
    }
}
