//
//  NotificationService.h
//  USPushExtension
//
//  Created by Norman van der Ploeg on 2017-03-06.
//  Copyright © 2017 Upsight. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
