//
//  NotificationService.m
//  USPushExtension
//
//  Created by Norman van der Ploeg on 2017-03-06.
//  Copyright © 2017 Upsight. All rights reserved.
//

#import "NotificationService.h"

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];

    // Get a reference to the Upsight data in the push payload
    NSDictionary * inPushPayload =  request.content.userInfo[@"upsight_push_data"];

    if(nil != inPushPayload && [inPushPayload isKindOfClass:[NSDictionary class]] ) {
        NSMutableDictionary * outPushPayload = [[NSMutableDictionary alloc] initWithDictionary:inPushPayload];
        // Create a string with the Image URL passed down in the
        // Upsight payload using the key "media_url"
        NSString * imgString = inPushPayload[@"media_url"];

        // If an image is contained in the payload, the code below will
        // Manage the downloading and displaying of the image in the push.
        if(nil != imgString && [imgString isKindOfClass:[NSString class]]) {
            NSURL * imgURL = [NSURL URLWithString:imgString];

            [[[NSURLSession sharedSession] downloadTaskWithURL:imgURL completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                // If the download is successful a copy of the data will be stored in a
                // temporary directory where the data can be used to display the push
                if(nil != location) {
                    NSString * aTmpDir = NSTemporaryDirectory();
                    NSString * aTmpFileString = [imgURL lastPathComponent];

                    NSURL * theTmpUrl = [NSURL URLWithString:[[@"file://" stringByAppendingString:aTmpDir] stringByAppendingString:aTmpFileString]];

                    bool didMove = [[NSFileManager defaultManager] moveItemAtURL:location toURL:theTmpUrl error:nil];
                    if(didMove) {
                        // The image is added to the push as an attachment.
                        UNNotificationAttachment * theAttachment = [UNNotificationAttachment attachmentWithIdentifier:@"USPushAttachment" URL:theTmpUrl options:nil error:nil];
                        self.bestAttemptContent.attachments = @[theAttachment];
                    }
                }
                else if(error) {
                    //  Uncomment this log if you are having issues with rich pushes.
                    //  NSLog("Error downloading file with Error:%@",error);
                    outPushPayload[@"error"] = error;
                    self.bestAttemptContent.userInfo = outPushPayload;
                }
                // Pass back the content handler that will display the push notification
                self.contentHandler(self.bestAttemptContent);
            }] resume];
        }
        else {
            // Append error details to the push payload
            outPushPayload[@"error"] = [NSError errorWithDomain:@"USPushDomain" code:0 userInfo:@{NSLocalizedDescriptionKey:@"No media URL in push payload"}];
            self.bestAttemptContent.userInfo = outPushPayload;
            self.contentHandler(self.bestAttemptContent);
        }
    }
    else {
        self.contentHandler(self.bestAttemptContent);
    }
}

- (void)serviceExtensionTimeWillExpire {
    // Deliver your "best attempt" content, otherwise the original push payload will be used.
    self.contentHandler(self.bestAttemptContent);
}

@end