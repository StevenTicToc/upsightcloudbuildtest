<manifest xmlns:android="http://schemas.android.com/apk/res/android" package="com.upsight.unityplugin">
	<uses-sdk android:minSdkVersion="14"/>

    <permission android:name="~~unity.package.id~~.permission.C2D_MESSAGE" android:protectionLevel="signature" />
    <uses-permission android:name="~~unity.package.id~~.permission.C2D_MESSAGE" />

    <application>
    	<meta-data android:name="com.upsight.sdk_plugin" android:value="unity:~~unity.plugin.version~~" />
        <meta-data android:name="com.upsight.app_token" android:value="ANDROID_APP_TOKEN" />
        <meta-data android:name="com.upsight.public_key" android:value="ANDROID_PUBLIC_KEY" />
        <meta-data android:name="com.upsight.gcm" android:value="GCM_SENDER_ID" />

        <provider
            android:name="com.upsight.android.internal.persistence.ContentProvider"
            android:authorities="~~unity.package.id~~.upsight"
            android:enabled="true"
            android:exported="false" />

        <receiver
            android:name="com.upsight.android.googlepushservices.internal.PushBroadcastReceiver"
            android:permission="com.google.android.c2dm.permission.SEND">
            <intent-filter>
                <action android:name="com.google.android.c2dm.intent.RECEIVE" />
                <category android:name="~~unity.package.id~~" />
            </intent-filter>
        </receiver>
    </application>
</manifest>
