/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

#if UNITY_ANDROID
using Google.JarResolver;
using UnityEditor;
using UpsightConfig = UpsightEditor.ProjectConfigurationWizard;
using UpsightPrefs = UpsightEditor.UpsightPrefs;

/// <summary>
/// This file outlines the Google Play Services and Android Support libraries that the Upsight Marketing module depends on.
/// </summary>
[InitializeOnLoad]
public static class UpsightMarketingDependencies
{
    public static readonly string PluginName = "UpsightMarketing";

    internal static PlayServicesSupport Resolver = null;

    public static bool IsEnabled;

    public static readonly string gmsVersion = "11.0.4";
    public static readonly string supportVersion = "26.0.2";

    public static readonly string[] gmsLibraries =
    {
        "play-services-basement",
    };

    //The commented libraries are required but are covered by a dependency
    public static readonly string[] supportLibraries =
    {
        //"support-annotations",
        //"support-v4",
    };


    static UpsightMarketingDependencies()
    {
        IsEnabled = UpsightPrefs.AndroidMarketingEnabled;

        if(IsEnabled)
            Init();
    }

    public static bool CheckIfNeedsResolving()
    {
        bool shouldEnable = UpsightPrefs.AndroidMarketingEnabled;
        if(IsEnabled != shouldEnable)
        {
            IsEnabled = shouldEnable;
            if(shouldEnable)
                Init();
            else
                Cleanup();
            return true;
        }

        bool needsResolving = false;
        if(IsEnabled)
        {
            foreach(var lib in gmsLibraries)
            {
                needsResolving |=
                    !UpsightConfig.IsNativeFileActive("Assets/Plugins/Android/" + lib + "-" + gmsVersion, BuildTarget.Android) &&
                    !UpsightConfig.IsNativeFileActive("Assets/Plugins/Android/" + lib + "-" + gmsVersion + ".aar", BuildTarget.Android) &&
                    !UpsightConfig.IsNativeFileActive("Assets/Plugins/Android/" + lib + "-" + gmsVersion + ".jar", BuildTarget.Android);
                if(needsResolving)
                    return true;
            }

            foreach(var lib in supportLibraries)
            {
                needsResolving |=
                    !UpsightConfig.IsNativeFileActive("Assets/Plugins/Android/" + lib + "-" + supportVersion, BuildTarget.Android) &&
                    !UpsightConfig.IsNativeFileActive("Assets/Plugins/Android/" + lib + "-" + supportVersion + ".aar", BuildTarget.Android) &&
                    !UpsightConfig.IsNativeFileActive("Assets/Plugins/Android/" + lib + "-" + supportVersion + ".jar", BuildTarget.Android);
                if(needsResolving)
                    return true;
            }
        }
        return needsResolving;
    }

    public static void ClearDependencies()
    {
        IsEnabled = false;
        if(Resolver != null)
            Resolver.ClearDependencies();
    }

    private static void Init()
    {
        Resolver = PlayServicesSupport.CreateInstance(PluginName, EditorPrefs.GetString("AndroidSdkRoot"), "ProjectSettings");
        Resolver.ClearDependencies();

        foreach(var lib in gmsLibraries)
            Resolver.DependOn("com.google.android.gms", lib, gmsVersion);

        foreach(var lib in supportLibraries)
            Resolver.DependOn("com.android.support", lib, supportVersion);
    }

    private static void Cleanup()
    {
        if(Resolver == null)
            return;

        Resolver.ClearDependencies();

        foreach(var lib in gmsLibraries)
        {
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + gmsVersion);
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + gmsVersion + ".aar");
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + gmsVersion + ".jar");
        }

        foreach(var lib in supportLibraries)
        {
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + supportVersion);
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + supportVersion + ".aar");
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + supportVersion + ".jar");
        }
    }
}

#endif
