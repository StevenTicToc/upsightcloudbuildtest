/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using UnityEditor;

namespace UpsightEditor
{
    public class UpsightPrefs : ScriptableObject
    {
        private const string PrefsFilePath = "Upsight.prefs";
        //private const string AndroidExtensionsKey = "AndroidExtensions"; //Deprecated
        private const string IosMediationEnabledKey = "IosMediationEnabled";
        private const string IosPushEnabledKey = "IosPushEnabled";
        private const string AndroidMarketingEnabledKey = "AndroidMarketingEnabled";
        private const string AndroidMediationEnabledKey = "AndroidMediationEnabled";
        private const string AndroidPushEnabledKey = "AndroidPushEnabled";

        private static UpsightPrefs _instance = null;
        private static string _absPathToUpsightFolder = null;
        private static Dictionary<string, string> _prefs = null;
        private static string _prefFileAbsPath = null;


        //Get the path to the Upsight folder (in case someone moved it)
        //This requires this script to be located in Upsight/Editor/**/UpsightPrefs.cs
        internal static string GetAbsPathToUpsightFolder()
        {
            if(_instance != null && _absPathToUpsightFolder != null)
                return _absPathToUpsightFolder;

            var monoScript = MonoScript.FromScriptableObject(_instance = CreateInstance<UpsightPrefs>());
            string scriptPath = AssetDatabase.GetAssetPath(monoScript) ?? string.Empty;
            scriptPath = scriptPath.Substring(6); //cut off Assets/
            scriptPath = scriptPath.Remove(scriptPath.LastIndexOf("Upsight/Editor/") + 8); //cut off "Editor/**/UpsightPrefs.cs"
            scriptPath = Application.dataPath + scriptPath;

            _absPathToUpsightFolder = scriptPath;
            return scriptPath;
        }

        //Stores whether iOS Mediaiton is enabled for use at build time
        internal static bool IosMediationEnabled
        {
            get
            {
                return GetBool(IosMediationEnabledKey, true);
            }

            set
            {
                Set(IosMediationEnabledKey, value);
            }
        }

        //Stores whether iOS Push is enabled for use at build time
        internal static bool IosPushEnabled
        {
            get
            {
                return GetBool(IosPushEnabledKey, true);
            }

            set
            {
                Set(IosPushEnabledKey, value);
            }
        }

        //Stores whether Android Marketing is enabled for use at build time
        internal static bool AndroidMarketingEnabled
        {
            get
            {
                return GetBool(AndroidMarketingEnabledKey, true);
            }

            set
            {
                Set(AndroidMarketingEnabledKey, value);
            }
        }

        //Stores whether Android Mediaiton is enabled for use at build time
        internal static bool AndroidMediationEnabled
        {
            get
            {
                return GetBool(AndroidMediationEnabledKey, true);
            }

            set
            {
                Set(AndroidMediationEnabledKey, value);
            }
        }

        //Stores whether Android Push is enabled for use at build time
        internal static bool AndroidPushEnabled
        {
            get
            {
                return GetBool(AndroidPushEnabledKey, true);
            }

            set
            {
                Set(AndroidPushEnabledKey, value);
            }
        }

        private static int GetInt(string key, int defaultValue)
        {
            int result;
            string value = _getValue(key);
            if(int.TryParse(value, out result))
                return result;
            else
                return defaultValue;
        }

        private static bool GetBool(string key, bool defaultValue)
        {
            bool result;
            string value = _getValue(key);
            if(bool.TryParse(value, out result))
                return result;
            else
                return defaultValue;
        }

        private static string GetString(string key)
        {
            return _getValue(key);
        }

        private static void Set<T>(string key, T value)
        {
            _writeValue(key, value.ToString());
        }

        ///// Helper Functions

        private static string _getValue(string key)
        {
            if(_prefs == null)
                _readPrefs();

            if(_prefs.ContainsKey(key))
                return _prefs[key];
            else
                return null;
        }

        private static void _writeValue(string key, string value)
        {
            if(_prefs == null)
                _readPrefs();

            if(_prefs.ContainsKey(key) && _prefs[key] != value)
            {
                _prefs[key] = value;
                _writePrefs();
            }
            else if(!_prefs.ContainsKey(key))
            {
                _prefs.Add(key, value);
                _writePrefs(new KeyValuePair<string, string>(key, value));
            }
        }

        private static void _readPrefs()
        {
            _prefs = new Dictionary<string, string>();
            _prefFileAbsPath = GetAbsPathToUpsightFolder() + PrefsFilePath;

            //If the file doesn't exist we'll create one when we store a value
            if(File.Exists(_prefFileAbsPath))
            {
                var lines = File.ReadAllLines(_prefFileAbsPath);
                foreach(var line in lines)
                {
                    var parts = line.Split(new char[] { '=' }, 2);
                    if(parts.Length == 2)
                        _prefs.Add(parts[0], parts[1]);
                }
            }
        }

        private static void _writePrefs(KeyValuePair<string, string>? appendPair = null)
        {
            if(appendPair != null)
                File.AppendAllText(_prefFileAbsPath, string.Format("{0}={1}{2}", appendPair.Value.Key, appendPair.Value.Value, Environment.NewLine));
            else
                File.WriteAllLines(_prefFileAbsPath, _prefs.Select(pair => string.Format("{0}={1}", pair.Key, pair.Value)).ToArray());
        }
    }
}
