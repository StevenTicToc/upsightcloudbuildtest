/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using UnityEditorCompat.iOS.Xcode;
using UnityEditorCompat.iOS.Xcode.Extensions;

namespace UpsightEditor
{
    public class UpsightPostProcessor : ScriptableObject
    {
        private const string FrameworkLocation = "System/Library/Frameworks/";
        private const string TbdLibLocation = "usr/lib/";

        private const string UpsightPushExtensionName = "USPushExtension";

        private const string PlistAdMediationUseSSLKey = "fuse_ssl";

        private const string PlistCalendarUsageKey = "NSCalendarsUsageDescription";
        private const string PlistCalendarUsageString = "Advertisement would like to create a calendar event.";
        private const string PlistPhotoLibraryUsageKey = "NSPhotoLibraryUsageDescription";
        private const string PlistPhotoLibraryUsageString = "Advertisement would like to store a photo.";
        private const string PlistCameraUsageKey = "NSCameraUsageDescription";
        private const string PlistCameraUsageString = "Advertisement would like to use your camera.";
        private const string PlistMotionUsageKey = "NSMotionUsageDescription";
        private const string PlistMotionUsageString = "Advertisement would like to use motion for interactive ad controls.";

        private const string PlistUIBGModesKey = "UIBackgroundModes";
        private const string PlistUIBGRemoteNotification = "remote-notification";

        private const string PlistAppQuerySchemasKey = "LSApplicationQueriesSchemes";

        private const string PlistATSKey = "NSAppTransportSecurity";
        private const string PlistAllowLocalNetworksKey = "NSAllowsLocalNetworking";
        private const string PlistAllowHTTPInWebContent = "NSAllowsArbitraryLoadsInWebContent";

        private static readonly string[] UpsightFrameworks = new string[]
        {
            "AdSupport.framework",
            "UserNotifications.framework",
            "StoreKit.framework",
            "CoreTelephony.framework",
            "CoreData.framework",
            "libxml2.tbd",
        };

        private static readonly string[] MediationFrameworks = new string[]
        {
            "Foundation.framework",
            "UIKit.framework",
            "MessageUI.framework",
            "EventKit.framework",
            "EventKitUI.framework",
            "Twitter.framework",
            "Social.framework",
            "Security.framework",
            "WebKit.framework",
            "GameKit.framework",
            "MobileCoreServices.framework",
            "SafariServices.framework",
            "WatchConnectivity.framework",
            "JavaScriptCore.framework",
            "libsqlite3.tbd",
            "libz.tbd",
        };

        private static readonly string[] QuerySchemasElements = new string[]
        {
            "fb",
            "instagram",
            "tumblr",
            "twitter",
        };


        [UnityEditor.MenuItem("Upsight/Manually run Xcode post processor", false, 21)]
        static void ManuallyRunPostProcessor()
        {
            string path;

            if(Application.platform == RuntimePlatform.OSXEditor)
                path = EditorUtility.OpenFilePanel("Locate your Xcode project file", Application.dataPath.Replace("/Assets", string.Empty), "xcodeproj");
            else
                path = EditorUtility.OpenFolderPanel("Locate your .xcodeproj folder", Application.dataPath.Replace("/Assets", string.Empty), string.Empty);

            if(!string.IsNullOrEmpty(path))
                OnPostProcessBuild(BuildTarget.iOS, path.EndsWith("xcodeproj") ? Directory.GetParent(path).FullName : path);
        }


        [PostProcessBuild(int.MaxValue)]
        private static void OnPostProcessBuild(BuildTarget target, string pathToBuiltProject)
        {
            var stopwatch = Stopwatch.StartNew();

            if(target == BuildTarget.iOS)
            {
                string absolutePath = UpsightPrefs.GetAbsPathToUpsightFolder();

                var settingsPlistPath = Path.Combine(absolutePath, ProjectConfigurationWizard.kIOSPlistPath);

                var uxmVariablesPlistFile = UpsightUtil.FileSystemUtil.GetLastFileOrFolderInPath(ProjectConfigurationWizard.kIOSUXMPlistPath);
                var uxmVariablesPlistPath = Path.Combine(absolutePath, ProjectConfigurationWizard.kIOSUXMPlistPath);
                var uxmVariablesPlistDestPath = Path.Combine(pathToBuiltProject, uxmVariablesPlistFile);

                var pushExtensionDir = Path.Combine(absolutePath, ProjectConfigurationWizard.kIOSPushExtensionDir);
                var pushExtensionDirDest = Path.Combine(pathToBuiltProject, UpsightUtil.FileSystemUtil.GetLastFileOrFolderInPath(ProjectConfigurationWizard.kIOSPushExtensionDir));
                bool pushExtensionMissing = false;

                // sanity check
                if(!File.Exists(settingsPlistPath))
                {
                    Debug.LogWarning("Could not find Upsight settings at " + settingsPlistPath + ". Please run the Tools -> Upsight -> Project Configuration Wizard menu item.");
                    return;
                }

                try
                {
                    if(File.Exists(uxmVariablesPlistPath))
                        File.Copy(uxmVariablesPlistPath, uxmVariablesPlistDestPath, true);
                    else
                        Debug.LogWarning("Could not find Upsight UXM Variables plist at " + uxmVariablesPlistPath + ". Please run the Tools -> Upsight -> Project Configuration Wizard menu item.");
                }
                catch(IOException e)
                {
                    Debug.LogWarning("Error while copying Upsight UXM Variables plist from "
                        + uxmVariablesPlistPath + " to " + uxmVariablesPlistDestPath + ". You may need to manually add this file to the xcode project.");
                    Debug.LogException(e);
                }

                if(UpsightPrefs.IosPushEnabled)
                {
                    try
                    {
                        if(Directory.Exists(pushExtensionDir))
                        {
                            if(!Directory.Exists(pushExtensionDirDest))
                                Directory.CreateDirectory(pushExtensionDirDest);
                            foreach(var file in Directory.GetFiles(pushExtensionDir).Where(f => !f.EndsWith(".meta")))
                                File.Copy(file, pushExtensionDirDest + "/" + UpsightUtil.FileSystemUtil.GetLastFileOrFolderInPath(file), true);
                        }
                        else
                        {
                            pushExtensionMissing = true;
                            Debug.LogWarning("Could not find Upsight Push Extension directory at " + pushExtensionDir + ". Please run the Tools -> Upsight -> Project Configuration Wizard menu item.");
                        }
                    }
                    catch(IOException e)
                    {
                        pushExtensionMissing = true;
                        Debug.LogWarning("Error while copying Upsight Push Extension directory from "
                            + pushExtensionDir + " to " + pushExtensionDirDest + ". You may need to manually add this file to the xcode project.");
                        Debug.LogException(e);
                    }
                }

                //Open .pbxproj
                string pbxProjPath = PBXProject.GetPBXProjectPath(pathToBuiltProject);
                var pbxProj = new PBXProject();
                pbxProj.ReadFromFile(pbxProjPath);

                string projectGuid = pbxProj.ProjectGuid();
                string targetGuid = pbxProj.TargetGuidByName(PBXProject.GetUnityTargetName());

                //Add frameworks
                foreach(var framework in UpsightFrameworks)
                {
                    if(pbxProj.ContainsFramework(targetGuid, framework, framework.EndsWith(".framework") ? FrameworkLocation : TbdLibLocation))
                        pbxProj.RemoveFrameworkFromProject(targetGuid, framework, framework.EndsWith(".framework") ? FrameworkLocation : TbdLibLocation);

                    pbxProj.AddFrameworkToProject(targetGuid, framework, true, framework.EndsWith(".framework") ? FrameworkLocation : TbdLibLocation);
                }

                if(UpsightPrefs.IosMediationEnabled)
                {
                    foreach(var framework in MediationFrameworks)
                    {
                        if(pbxProj.ContainsFramework(targetGuid, framework, framework.EndsWith(".framework") ? FrameworkLocation : TbdLibLocation))
                            pbxProj.RemoveFrameworkFromProject(targetGuid, framework, framework.EndsWith(".framework") ? FrameworkLocation : TbdLibLocation);

                        pbxProj.AddFrameworkToProject(targetGuid, framework, true, framework.EndsWith(".framework") ? FrameworkLocation : TbdLibLocation);
                    }

                    Directory.CreateDirectory(pathToBuiltProject + "/FrameworkRes");
                    var iosLibsPath = Path.Combine(absolutePath, ProjectConfigurationWizard.kIOSCurrentVersionPath);
                    var frameworks = Directory.GetDirectories(iosLibsPath, ProjectConfigurationWizard.kIOSMediationLib, SearchOption.AllDirectories);
                    foreach(string frameworkPath in frameworks)
                        if(Directory.Exists(frameworkPath + "/Resources"))
                            UpsightUtil.FileSystemUtil.RecursiveDirCopy(frameworkPath + "/Resources", pathToBuiltProject + "/FrameworkRes");

                    //Don't iterate recursively.
                    //Instead add all files and folders in the topmost directory as files.
                    //This adds .bundle folders as files which is what's needed.
                    foreach(string path in Directory.GetFiles(pathToBuiltProject + "/FrameworkRes", "*", SearchOption.TopDirectoryOnly))
                    {
                        string localPath = path.Substring(pathToBuiltProject.Length + 1);
                        pbxProj.AddFileToBuild(targetGuid, pbxProj.AddFile(localPath, localPath));
                    }

                    foreach(string path in Directory.GetDirectories(pathToBuiltProject + "/FrameworkRes", "*", SearchOption.TopDirectoryOnly))
                    {
                        string localPath = path.Substring(pathToBuiltProject.Length + 1);
                        pbxProj.AddFileToBuild(targetGuid, pbxProj.AddFile(localPath, localPath));
                    }
                }

                if(UpsightPrefs.IosPushEnabled && !pushExtensionMissing)
                {
                    //Add USPushExtension for Image Push if it doesn't exist yet
                    if(pbxProj.TargetGuidByName(UpsightPushExtensionName) == null)
                    {
                        var pushExtensionGuid = pbxProj.AddAppExtension(targetGuid, UpsightPushExtensionName, ProjectConfigurationWizard.IosBundleId + "." + UpsightPushExtensionName, ProjectConfigurationWizard.kIOSPushExtensionPlist);
                        pbxProj.AddFrameworkToProject(pushExtensionGuid, "UserNotifications.framework", true);
                        var pushExtensionFiles = Directory.GetFiles(pushExtensionDirDest)
                            .Select(f => UpsightUtil.FileSystemUtil.GetLastFileOrFolderInPath(pushExtensionDirDest) + "/" + UpsightUtil.FileSystemUtil.GetLastFileOrFolderInPath(f));
                        foreach(var file in pushExtensionFiles)
                            pbxProj.AddFileToBuild(pushExtensionGuid, pbxProj.AddFile(file, file));

                        //Sometimes the main project forgets about arm64
                        pbxProj.UpdateBuildProperty(new string[] { projectGuid, pushExtensionGuid }, "ARCHS", new string[] { "arm64" }, new string[] { });

                        //Set the target version for push to 10.0
                        pbxProj.AddBuildProperty(pushExtensionGuid, "IPHONEOS_DEPLOYMENT_TARGET", "10.0");
                    }
                }

#if UNITY_5_4_OR_NEWER
                //Set the development team for all the targets
                pbxProj.SetTargetAttributes("DevelopmentTeam", PlayerSettings.iOS.appleDeveloperTeamID);
#endif

                //Add the uxmVariables file
                pbxProj.AddFileToBuild(targetGuid, pbxProj.AddFile(uxmVariablesPlistFile, uxmVariablesPlistFile));

                //Add -ObjC linker flag
                pbxProj.AddBuildProperty(targetGuid, "OTHER_LDFLAGS", "-ObjC");

                pbxProj.WriteToFile(pbxProjPath);

                if(UpsightPrefs.IosPushEnabled)
                {
                    //Add push capabilities, code signing entitlements also added here
                    var capabilityManager = new ProjectCapabilityManager(pbxProjPath, ProjectConfigurationWizard.IosBundleId + ".entitlements", PBXProject.GetUnityTargetName());
                    capabilityManager.AddPushNotifications(EditorUserBuildSettings.development);
                    capabilityManager.WriteToFile();
                }

                //Merge Plist files
                string plistPath = Path.Combine(pathToBuiltProject, "Info.plist");
                var basePlist = new PlistDocument();
                basePlist.ReadFromFile(plistPath);

                var upsightPlist = new PlistDocument();
                upsightPlist.ReadFromFile(settingsPlistPath);

                basePlist.MergeAdditionalPlist(upsightPlist);

                if(UpsightPrefs.IosPushEnabled)
                {
                    //Add remote push notifications. This can't be merged in because the array may already exist
                    PlistElementArray bgModesArray = (basePlist.root[PlistUIBGModesKey] ?? (basePlist.root[PlistUIBGModesKey] = new PlistElementArray())).AsArray();
                    bgModesArray.AddStringIfMissing(PlistUIBGRemoteNotification);
                }

                //Mediation entires
                if(UpsightPrefs.IosMediationEnabled)
                {
                    //Use SSL
                    basePlist.root[PlistAdMediationUseSSLKey] = new PlistElementBoolean(true);

                    //Add permission text
                    basePlist.root[PlistCalendarUsageKey] = new PlistElementString(PlistCalendarUsageString);
                    basePlist.root[PlistPhotoLibraryUsageKey] = new PlistElementString(PlistPhotoLibraryUsageString);
                    basePlist.root[PlistCameraUsageKey] = new PlistElementString(PlistCameraUsageString);
                    basePlist.root[PlistMotionUsageKey] = new PlistElementString(PlistMotionUsageString);

                    PlistElementDict atsDict = (basePlist.root[PlistATSKey] ?? (basePlist.root[PlistATSKey] = new PlistElementDict())).AsDict();
                    atsDict[PlistAllowLocalNetworksKey] = new PlistElementBoolean(true);
                    atsDict[PlistAllowHTTPInWebContent] = new PlistElementBoolean(true);

                    PlistElementArray appQueriesSchemes = (basePlist.root[PlistAppQuerySchemasKey] ?? (basePlist.root[PlistAppQuerySchemasKey] = new PlistElementArray())).AsArray();
                    foreach(var s in QuerySchemasElements)
                        appQueriesSchemes.AddStringIfMissing(s);
                }

                basePlist.WriteToFile(plistPath);

                stopwatch.Stop();
                Debug.Log("Upsight iOS post processor completed successfully in " + stopwatch.ElapsedMilliseconds + "ms.");
            }
        }
    }

    public static class PlistParserExtnesions
    {
        public static void MergeAdditionalPlist(this PlistDocument basePlistDoc, PlistDocument additionalPlistDoc)
        {
            if(additionalPlistDoc == null)
                return;

            foreach(var val in additionalPlistDoc.root.AsDict().values)
                basePlistDoc.root[val.Key] = val.Value;
        }

        public static void AddStringIfMissing(this PlistElementArray array, string newString)
        {
            if(!array.values.Any(e => e.AsString() == newString))
                array.AddString(newString);
        }
    }
}
