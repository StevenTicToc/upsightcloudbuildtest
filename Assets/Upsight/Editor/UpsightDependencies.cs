/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

#if UNITY_ANDROID
using Google.JarResolver;
using UnityEditor;

public class UpsightDependencies : AssetPostprocessor
{
    private static readonly string PluginName = "UpsightDependencyResolver";
    private static readonly PlayServicesSupport resolver;

    public static readonly string gmsVersion = "11.0.4";
    public static readonly string supportVersion = "26.0.2";

    public static readonly string[] gmsLibraries =
    {
        "play-services-basement",    //marketing
        "play-services-base",        //push
        "play-services-gcm",         //push
        "play-services-iid",         //push
        "play-services-tasks",       //push
    };

    public static readonly string[] supportLibraries =
    {
        "support-annotations", //all
        "support-v4",          //all
    };


    static UpsightDependencies()
    {
        resolver = PlayServicesSupport.CreateInstance(PluginName, EditorPrefs.GetString("AndroidSdkRoot"), "ProjectSettings");
    }

    public static bool UpdateDependencies()
    {
        bool needsResolving = false;
        needsResolving |= UpsightMarketingDependencies.CheckIfNeedsResolving();
        needsResolving |= UpsightMediationDependencies.CheckIfNeedsResolving();
        needsResolving |= UpsightPushDependencies.CheckIfNeedsResolving();
        return needsResolving;
    }
    
    public static void ResolveDependencies(System.Action<bool> resolveCompleted = null)
    {
        GooglePlayServices.PlayServicesResolver.Resolve(forceResolution: true, resolutionCompleteWithResult: (success) => { _resolutionFinished(success); if(resolveCompleted != null) resolveCompleted(success); });
    }

    private static void _resolutionFinished(bool success)
    {
        if(!success)
            return;

        AssetDatabase.Refresh();

        foreach(var lib in gmsLibraries)
        {
            UpsightEditor.ProjectConfigurationWizard.SetNativeFileActiveState("Assets/Plugins/Android/" + lib + "-" + gmsVersion, BuildTarget.Android, true);
            UpsightEditor.ProjectConfigurationWizard.SetNativeFileActiveState("Assets/Plugins/Android/" + lib + "-" + gmsVersion + ".aar", BuildTarget.Android, true);
            UpsightEditor.ProjectConfigurationWizard.SetNativeFileActiveState("Assets/Plugins/Android/" + lib + "-" + gmsVersion + ".jar", BuildTarget.Android, true);
        }

        foreach(var lib in supportLibraries)
        {
            UpsightEditor.ProjectConfigurationWizard.SetNativeFileActiveState("Assets/Plugins/Android/" + lib + "-" + supportVersion, BuildTarget.Android, true);
            UpsightEditor.ProjectConfigurationWizard.SetNativeFileActiveState("Assets/Plugins/Android/" + lib + "-" + supportVersion + ".aar", BuildTarget.Android, true);
            UpsightEditor.ProjectConfigurationWizard.SetNativeFileActiveState("Assets/Plugins/Android/" + lib + "-" + supportVersion + ".jar", BuildTarget.Android, true);
        }

        AssetDatabase.Refresh();
    }

    // Handle delayed loading of the dependency resolvers.
    private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromPath)
    {
        foreach(string asset in importedAssets)
        {
            if(asset.Contains("JarResolver") && UpdateDependencies())
            {
                ResolveDependencies();
                break;
            }
        }
    }

    [MenuItem("Upsight/Resolve Google Dependencies", false, 22)]
    public static void RunUpsightResolver()
    {
        UpdateDependencies();
        ResolveDependencies();
    }

    [MenuItem("Upsight/Delete All Google Dependencies", false, 23)]
    public static void DeleteExtensionDependencies()
    {
        //Delete dependency xml files so that they have to be recreated
        resolver.ClearDependencies();
        UpsightMarketingDependencies.ClearDependencies();
        UpsightMediationDependencies.ClearDependencies();
        UpsightPushDependencies.ClearDependencies();

        foreach(var lib in gmsLibraries)
        {
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + gmsVersion);
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + gmsVersion + ".aar");
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + gmsVersion + ".jar");
        }

        foreach(var lib in supportLibraries)
        {
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + supportVersion);
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + supportVersion + ".aar");
            AssetDatabase.DeleteAsset("Assets/Plugins/Android/" + lib + "-" + supportVersion + ".jar");
        }

        AssetDatabase.Refresh();
    }
}

#endif
