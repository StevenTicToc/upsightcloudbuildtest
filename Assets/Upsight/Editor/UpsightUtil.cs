﻿/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using System;
using System.IO;

namespace UpsightUtil
{
    public static class FileSystemUtil
    {
        //Recursively copy an entire directory to another location. Includes options to skip empty folders and overwrite files
        //Returns whether any files/folders were actually copied
        public static bool RecursiveDirCopy(string fromPath, string toPath, bool skipEmpty = true, bool overwrite = true)
        {
            var files = Directory.GetFiles(fromPath);
            var dirs = Directory.GetDirectories(fromPath);

            if((!skipEmpty || files.Length > 0) && !Directory.Exists(toPath))
                Directory.CreateDirectory(toPath);

            foreach(var file in files)
                File.Copy(file, toPath + "/" + GetLastFileOrFolderInPath(file), overwrite);

            bool didCopy = false;
            foreach(var dir in dirs)
                didCopy |= RecursiveDirCopy(dir, toPath + "/" + GetLastFileOrFolderInPath(dir), skipEmpty, overwrite);

            return didCopy || files.Length > 0 || !skipEmpty;
        }

        public static string GetLastFileOrFolderInPath(string path)
        {
            return path.Substring(path.LastIndexOfAny(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar }) + 1);
        }
    }
}