﻿/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

#if UNITY_ANDROID
using Google.JarResolver;
using UnityEditor;

/// <summary>
/// This file outlines the Google Play Services and Android Support libraries that the Upsight SDK depends on.
/// </summary>
[InitializeOnLoad]
public static class UpsightCoreDependencies
{
    public static readonly string PluginName = "UpsightCore";

    private static readonly PlayServicesSupport Resolver;

    public static readonly string supportVersion = "26.0.2";

    public static readonly string[] supportLibraries =
    {
        "support-annotations",
        "support-v4",
    };


    static UpsightCoreDependencies()
    {
        Resolver = PlayServicesSupport.CreateInstance(PluginName, EditorPrefs.GetString("AndroidSdkRoot"), "ProjectSettings");
        Resolver.ClearDependencies();

        foreach(var lib in supportLibraries)
            Resolver.DependOn("com.android.support", lib, supportVersion);
    }
}

#endif
