/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using System.Linq;
using UnityEditorInternal;
using UpsightUtil;
using UnityEditorCompat.iOS.Xcode;
using UpsightMiniJSON;

namespace UpsightEditor
{
    public class ProjectConfigurationWizard : EditorWindow
    {
        #region Constants

        //These 3 version lines are modified by script. DO NOT RENAME!
        public const string UNITY_PLUGIN_VERSION = /* matches unity.properties */ "4.9.3";
        public const string ANDROID_SDK_VERSION = /* matches unity.properties */ "4.9.3";
        public const string IOS_SDK_VERSION = /* matches unity.properties */ "4.9.3";

        // iOS keys
        private const string kUpsightAppTokenKey = "UpsightAppToken";
        private const string kUpsightPublicKey = "UpsightPublicKey";
        private const string kUpsightUserAttributesKey = "UpsightUserAttributes";
        private const string kManagedVariableTagKey = "tag";
        private const string kManagedVariableDefaultValueKey = "default";
        private const string kManagedVariableDescriptionKey = "description";
        private const string kManagedVariableTypeKey = "type";
        private const string kManagedVariableMinKey = "min";
        private const string kManagedVariableMaxKey = "max";

        // iOS paths
        internal const string kIOSPlistPath = "NativeLibs/iOS/settings.plist";
        internal const string kIOSUXMPlistPath = "NativeLibs/iOS/UpsightUXMVariables.plist";
        internal const string kIOSPushExtensionDir = "NativeLibs/iOS/USPushExtension";
        internal const string kIOSPushExtensionPlist = "USPushExtension/USPushExtensionInfo.plist";
        internal const string kIOSFolderPath = "NativeLibs/iOS/";
        internal const string kIOSCurrentVersionPath = "NativeLibs/iOS/" + IOS_SDK_VERSION;
        internal const string kIOSMediationLib = "UpsightMediation*.framework";

        // Android keys
        private const string kManifestAppTokenName = "com.upsight.app_token";
        private const string kManifestPublicKeyName = "com.upsight.public_key";
        private const string kManifestUserAttributeNamePrefix = "com.upsight.user_attribute.";
        private const string kManifestGcmName = "com.upsight.gcm";
        private const string kAndroidXmlElementName = "android:name";
        private const string kAndroidXmlElementValue = "android:value";
        private const string kAndroidNamespaceUri = "http://schemas.android.com/apk/res/android";
        private const string kPackageNameKey = "~~unity.package.id~~";
        private const string kPluginVersionKey = "~~unity.plugin.version~~";

        // Android paths
        internal const string kAndroidLibrariesPath = "NativeLibs/Android/" + ANDROID_SDK_VERSION;
        private const string kAndroidLibrariesX86Path = kAndroidLibrariesPath + "/jni/x86";
        private const string kAndroidLibrariesArmv7Path = kAndroidLibrariesPath + "/jni/armeabi-v7a";
        private const string kAndroidManifestTemplatePath = "NativeLibs/Android/ManifestTemplate.txt";
        private const string kAndroidPluginsDataPath = "/Plugins/Android/upsight-data";
        private const string kAndroidPluginsDataProjectPropertiesName = "/project.properties";
        private const string kAndroidPluginsDataProjectPropertiesContents = "android.library=true";
        private const string kAndroidManifestPath = "/AndroidManifest.xml";
        private const string kAndroidUXMJsonDirPath = "/res/raw";
        private const string kAndroidUXMJsonName = "/uxm_schema.json";
        private const string kAndroidIconDirPath = "/res/drawable";
        internal const string kAndroidIconName = "/upsight_notification_icon.png";

        // Other consts
        private const string kDefaultDate = "9999-12-31T23:59:59Z";
        private const int kAndroidPushIconHeight = 72;
        private const int kAndroidPushIconWidth = 72;

        #endregion

        #region Members

        // Members
        private string _rootFolderPath;
        private string _rootFolderAbsPath;
        private string _iosPlistPath;
        private string _androidManifestPath;
        private string _iosManagedVariablesPath;
        private string _androidManagedVariablesPath;
        private string _iosAppToken;
        private string _iosPublicKey;
        private string _androidAppToken;
        private string _androidPublicKey;
        private string _androidGcmSenderId;
        private string _error;
        private string _newIconPath;
        private bool _iosMediationEnabled;
        private bool _iosPushEnabled;
        private bool _androidMarketingEnabled;
        private bool _androidMediationEnabled;
        private bool _androidPushEnabled;
        private bool _iconFoldout = false;
        private bool _userAttributesFoldout = false;
        private bool _managedVariablesFoldout = false;
        private Texture2D _icon;
        private Vector2 _scrollPosition;
        private List<UserAttribute> _iosUserAttributes = new List<UserAttribute>();
        private List<UserAttribute> _androidUserAttributes = new List<UserAttribute>();
        private List<ManagedVariable> _iosManagedVariables = new List<ManagedVariable>();
        private List<ManagedVariable> _androidManagedVariables = new List<ManagedVariable>();
        private ReorderableList _androidReorderableList;
        private ReorderableList _iosReorderableList;
        private ReorderableList _androidManagedVariablesReorderableList;
        private ReorderableList _iosManagedVariablesReorderableList;

        #endregion

        #region Editor Window

        [UnityEditor.MenuItem("Upsight/Project Configuration Wizard", false, 1)]
        static void Init()
        {
            // Create the window
            var wizard = EditorWindow.GetWindow<ProjectConfigurationWizard>(true, "Upsight Project Configuration Editor", true);
            wizard.minSize = new Vector2(520, 540);
            wizard.maxSize = new Vector2(520, 1200);

            wizard.initData();
            wizard.setupLists();
        }

        //Run the Project Configuration Wizard automatically setting the provided parameters
        //This is meant to be run from an automated script
        public static void ConfigureProject(string iosAppToken, string iosPublicKey, string androidAppToken, string androidPublicKey, string gcmSenderID,
            UpsightIosExtensions iosExtensions = UpsightIosExtensions.Mediation | UpsightIosExtensions.Push,
            UpsightAndroidExtensions androidExtensions = UpsightAndroidExtensions.Mediation | UpsightAndroidExtensions.Push,
            bool clearExistingAndroidUserAttributes = false, List<UserAttribute> androidUserAttributes = null,
            bool clearExistingIosUserAttributes = false, List<UserAttribute> iosUserAttributes = null,
            bool clearExistingAndroidManagedVariables = false, List<ManagedVariable> androidManagedVariables = null,
            bool clearExistingIosManagedVariables = false, List<ManagedVariable> iosManagedVariables = null)
        {
            var wizard = EditorWindow.GetWindow<ProjectConfigurationWizard>("", false);

            wizard.initData();

            //Set with function paramaters
            wizard._iosAppToken = iosAppToken;
            wizard._iosPublicKey = iosPublicKey;
            wizard._androidAppToken = androidAppToken;
            wizard._androidPublicKey = androidPublicKey;
            wizard._androidGcmSenderId = gcmSenderID;

            wizard._androidMarketingEnabled = (androidExtensions & UpsightAndroidExtensions.Marketing) != 0;
            wizard._androidMediationEnabled = (androidExtensions & UpsightAndroidExtensions.Mediation) != 0;
            wizard._androidPushEnabled = (androidExtensions & UpsightAndroidExtensions.Push) != 0;
            wizard._iosMediationEnabled = (iosExtensions & UpsightIosExtensions.Mediation) != 0;
            wizard._iosPushEnabled = (iosExtensions & UpsightIosExtensions.Push) != 0;

            if(clearExistingAndroidUserAttributes)
                wizard._androidUserAttributes.Clear();
            if(androidUserAttributes != null)
                wizard._androidUserAttributes.AddRange(androidUserAttributes);

            if(clearExistingIosUserAttributes)
                wizard._iosUserAttributes.Clear();
            if(iosUserAttributes != null)
                wizard._iosUserAttributes.AddRange(iosUserAttributes);

            if(clearExistingAndroidManagedVariables)
                wizard._androidManagedVariables.Clear();
            if(androidManagedVariables != null)
                wizard._androidManagedVariables.AddRange(androidManagedVariables);

            if(clearExistingIosManagedVariables)
                wizard._iosManagedVariables.Clear();
            if(iosManagedVariables != null)
                wizard._iosManagedVariables.AddRange(iosManagedVariables);

            //Save everything and quit
            wizard.saveData();
            wizard.closeWindow();
        }

        private void initData()
        {
            _rootFolderAbsPath = UpsightPrefs.GetAbsPathToUpsightFolder();
            _androidMarketingEnabled = UpsightPrefs.AndroidMarketingEnabled;
            _androidMediationEnabled = UpsightPrefs.AndroidMediationEnabled;
            _androidPushEnabled = UpsightPrefs.AndroidPushEnabled;
            _iosMediationEnabled = UpsightPrefs.IosMediationEnabled;
            _iosPushEnabled = UpsightPrefs.IosPushEnabled;

            iosReadSettingsPlist();
            androidReadManifest();
            iosReadManagedVariables();
            androidReadManagedVariables();

            var iosMediationFrameworks = Directory.GetDirectories(_rootFolderAbsPath + kIOSCurrentVersionPath, kIOSMediationLib);

            if(iosMediationFrameworks.Length == 0)
                Debug.LogWarning("Upsight SDK: IOS Ad Mediation Library is missing!");
            else
                for(int i = 0; i < iosMediationFrameworks.Length; i++)
                    foreach(var innerFramework in Directory.GetDirectories(iosMediationFrameworks[i], "*.framework", SearchOption.AllDirectories))
                        _iosMediationEnabled |= IsNativeFileActive(innerFramework, BuildTarget.iOS);
        }

        private void saveData()
        {
            //Trim whitespace on keys and tokens
            _iosAppToken = string.IsNullOrEmpty(_iosAppToken) ? _iosAppToken : _iosAppToken.Trim();
            _iosPublicKey = string.IsNullOrEmpty(_iosPublicKey) ? _iosPublicKey : _iosPublicKey.Trim();
            _androidAppToken = string.IsNullOrEmpty(_androidAppToken) ? _androidAppToken : _androidAppToken.Trim();
            _androidPublicKey = string.IsNullOrEmpty(_androidPublicKey) ? _androidPublicKey : _androidPublicKey.Trim();
            _androidGcmSenderId = string.IsNullOrEmpty(_androidGcmSenderId) ? _androidGcmSenderId : _androidGcmSenderId.Trim();

            iosSaveSettingsPlist();
            androidSaveManifest();
            iosSaveManagedVariables();
            androidSaveManagedVariables();
            iosApplyExtensionSettings();
            androidApplyExtensionSettings();
#if UNITY_ANDROID
            bool needUpdate = UpsightDependencies.UpdateDependencies();
            if(needUpdate)
                UpsightDependencies.ResolveDependencies();
#endif
        }

        private void closeWindow()
        {
            if(_icon != null)
                DestroyImmediate(_icon, true);
            _icon = null;

            Close();
        }

        void OnGUI()
        {
            // dont attempt to layout until we have data
            if(_iosReorderableList == null || _androidReorderableList == null)
                return;

            _scrollPosition = GUILayout.BeginScrollView(_scrollPosition);
            {
                EditorStyles.textField.wordWrap = true;
                GUILayout.Label("iOS Upsight App Details", EditorStyles.miniButtonMid);
                GUILayout.Space(5);
                _iosAppToken = EditorGUILayout.TextField("App Token", _iosAppToken);
                GUILayout.Label("Public Key");
                _iosPublicKey = EditorGUILayout.TextArea(_iosPublicKey, GUILayout.Height(90));
                if(!ValidatePublicKey(_iosPublicKey))
                    EditorGUILayout.HelpBox("Invalid Public key. Your Public Key can be found on the Upsight dashboard. It will start with 'MII'", MessageType.Error);

                GUILayout.Space(20);

                GUILayout.Label("Android Upsight App Details", EditorStyles.miniButtonMid);
                GUILayout.Space(5);
                _androidAppToken = EditorGUILayout.TextField("App Token", _androidAppToken);
                GUILayout.Label("Public Key");
                _androidPublicKey = EditorGUILayout.TextArea(_androidPublicKey, GUILayout.Height(90));
                if(!ValidatePublicKey(_androidPublicKey))
                    EditorGUILayout.HelpBox("Invalid Public key. Your Public Key can be found on the Upsight dashboard. It will start with 'MII'", MessageType.Error);
                GUILayout.Space(5);
                _androidGcmSenderId = EditorGUILayout.TextField("GCM Sender ID", _androidGcmSenderId);

                GUILayout.Space(10);
                
                float oldLabelWidth = EditorGUIUtility.labelWidth;
                
                GUILayout.BeginHorizontal("iOS Extensions:", new GUIStyle(EditorStyles.helpBox) { fontSize = 12, fontStyle = FontStyle.Bold });
                EditorGUILayout.LabelField(new GUIContent("               ", "Select the capabilities to include in iOS builds"));
                GUILayout.FlexibleSpace();
                EditorGUIUtility.labelWidth = 80;
                _iosMediationEnabled = EditorGUILayout.Toggle("Ad Mediation", _iosMediationEnabled);
                EditorGUIUtility.labelWidth = 100;
                _iosPushEnabled = EditorGUILayout.Toggle("Push Notification", _iosPushEnabled);
                GUILayout.EndHorizontal();

                GUILayout.Space(5);
                
                GUILayout.BeginHorizontal("Android Extensions:", new GUIStyle(EditorStyles.helpBox) { fontSize = 12, fontStyle = FontStyle.Bold });
                EditorGUILayout.LabelField(new GUIContent("               ", "Select the capabilities to include in Android builds"));
                GUILayout.FlexibleSpace();
                EditorGUIUtility.labelWidth = 80;
                GUI.enabled = !(_androidPushEnabled || _androidMediationEnabled);
                _androidMarketingEnabled = EditorGUILayout.Toggle("Marketing", _androidMarketingEnabled || _androidPushEnabled || _androidMediationEnabled);
                GUI.enabled = true;
                _androidMediationEnabled = EditorGUILayout.Toggle("Ad Mediation", _androidMediationEnabled);
                EditorGUIUtility.labelWidth = 100;
                _androidPushEnabled = EditorGUILayout.Toggle("Push Notification", _androidPushEnabled);
                GUILayout.EndHorizontal();

                EditorGUIUtility.labelWidth = oldLabelWidth;

                GUILayout.Space(10);

                if(_userAttributesFoldout = EditorGUILayout.Foldout(_userAttributesFoldout, "User Attributes"))
                {
                    _iosReorderableList.DoLayoutList();

                    GUILayout.Space(5);

                    _androidReorderableList.DoLayoutList();
                }

                GUILayout.Space(10);

                if(_managedVariablesFoldout = EditorGUILayout.Foldout(_managedVariablesFoldout, "Managed Variables"))
                {
                    _iosManagedVariablesReorderableList.DoLayoutList();

                    GUILayout.Space(5);

                    _androidManagedVariablesReorderableList.DoLayoutList();
                }

                GUILayout.Space(10);

                if(_iconFoldout = EditorGUILayout.Foldout(_iconFoldout, "Android notification icon"))
                {
                    string iconPath = Application.dataPath + kAndroidPluginsDataPath + kAndroidIconDirPath;

                    //Note iconPath may not necessarily exist. If the user selects a valid file we will create it then.

                    if(_icon == null)
                    {
                        _icon = new Texture2D(kAndroidPushIconWidth, kAndroidPushIconHeight);
                        if(!string.IsNullOrEmpty(iconPath) && Directory.Exists(iconPath) && File.Exists(iconPath + kAndroidIconName))
                            _icon.LoadImage(File.ReadAllBytes(iconPath + kAndroidIconName));
                    }

                    GUILayout.Space(10);

                    if(GUILayout.Button("Click to select icon:", EditorStyles.label))
                    {
                        _newIconPath = EditorUtility.OpenFilePanel("Choose icon", Application.dataPath, "png");
                    }

                    GUILayout.BeginHorizontal();
                    if(GUILayout.Button(_icon, EditorStyles.objectFieldThumb, GUILayout.Height(kAndroidPushIconHeight + 3), GUILayout.Width(kAndroidPushIconWidth + 3)))
                    {
                        _newIconPath = EditorUtility.OpenFilePanel("Choose icon", Application.dataPath, "png");
                    }

                    if(_error == null)
                        EditorGUILayout.HelpBox("Your image must be " + kAndroidPushIconWidth + "x" + kAndroidPushIconHeight + " pixels", MessageType.None);
                    else
                        EditorGUILayout.HelpBox(_error, MessageType.Error);

                    GUILayout.EndHorizontal();

                    if(!string.IsNullOrEmpty(iconPath) && !string.IsNullOrEmpty(_newIconPath) && _newIconPath != (iconPath + kAndroidIconName))
                    {
                        updateAndroidPushIcon(iconPath, kAndroidIconName);
                    }
                    else
                    {
                        _newIconPath = null;
                        _error = null;
                    }
                }
                else if(_icon != null)
                {
                    DestroyImmediate(_icon);
                    _icon = null;
                }


                GUILayout.Space(20);

                GUILayout.BeginHorizontal();
                {
                    GUILayout.Space(250);

                    if(GUILayout.Button("Cancel"))
                        closeWindow();

                    if(GUILayout.Button("Save Changes"))
                    {
                        if(validateUserAttributes() && validateManagedVariables())
                        {
                            saveData();
                            closeWindow();
                        }
                        else
                        {
                            EditorUtility.DisplayDialog("Warning!", "Ensure that your user attributes all have keys and values before saving!", "OK");
                        }
                    }
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndScrollView();
        }

        #endregion

        #region iOS Settings Plist Manipulation

        private void iosReadSettingsPlist()
        {
            _iosPlistPath = _rootFolderAbsPath + kIOSPlistPath;
            if(!File.Exists(_iosPlistPath))
            {
                Debug.Log("iOS Settings File not found at " + _iosPlistPath + ". A new file will be created when you click Save.");

                _iosAppToken = "IOS_APP_TOKEN";
                _iosPublicKey = "IOS_PUBLIC_KEY";
            }
            else
            {
                var plist = new PlistDocument();
                plist.ReadFromFile(_iosPlistPath);

                var dict = plist.root as PlistElementDict;
                foreach(var val in dict.values)
                {
                    if(val.Key == kUpsightAppTokenKey)
                    {
                        _iosAppToken = val.Value.AsString();
                    }
                    else if(val.Key == kUpsightPublicKey)
                    {
                        _iosPublicKey = val.Value.AsString();
                    }
                    else if(val.Key == kUpsightUserAttributesKey)
                    {
                        var attrDict = val.Value.AsDict().values;
                        foreach(var ele in attrDict)
                        {
                            string typename;
                            if(ele.Value is PlistElementBoolean)
                                typename = SerializableTypeStrings[(int)SerializableTypes.Bool];
                            else if(ele.Value is PlistElementFloat)
                                typename = SerializableTypeStrings[(int)SerializableTypes.Float];
                            else if(ele.Value is PlistElementInteger)
                                typename = SerializableTypeStrings[(int)SerializableTypes.Int];
                            else if(ele.Value is PlistElementDateString)
                                typename = SerializableTypeStrings[(int)SerializableTypes.DateTime];
                            else
                                typename = SerializableTypeStrings[(int)SerializableTypes.String];

                            _iosUserAttributes.Add(new UserAttribute(typename + "." + ele.Key, ele.Value.ToString()));
                        }
                    }
                } // end foreach
            }
        }

        private void iosSaveSettingsPlist()
        {
            var plist = new PlistDocument();
            plist.root[kUpsightAppTokenKey] = new PlistElementString(_iosAppToken);
            plist.root[kUpsightPublicKey] = new PlistElementString(_iosPublicKey);

            if(_iosUserAttributes != null && _iosUserAttributes.Count > 0)
            {
                var plistDict = new PlistElementDict();

                foreach(var attr in _iosUserAttributes)
                {
                    try
                    {
                        if(attr.AttributeType == SerializableTypes.Bool)
                            plistDict.SetBoolean(attr.Name, bool.Parse(attr.StringValue));
                        else if(attr.AttributeType == SerializableTypes.Int)
                            plistDict.SetInteger(attr.Name, int.Parse(attr.StringValue));
                        else if(attr.AttributeType == SerializableTypes.Float)
                            plistDict.SetFloat(attr.Name, float.Parse(attr.StringValue));
                        else if(attr.AttributeType == SerializableTypes.DateTime)
                            plistDict.SetDateString(attr.Name, attr.StringValue);
                        else
                            plistDict.SetString(attr.Name, attr.StringValue);
                    }
                    catch(Exception e)
                    {
                        // catch any parse errors
                        Debug.LogError("Failed to write iOS User Attribute " + attr.Name + ": " + e);
                    }
                }

                plist.root[kUpsightUserAttributesKey] = plistDict;
            }

            plist.WriteToFile(_iosPlistPath);
        }

        #endregion

        #region iOS Managed Variables Manipulation

        private void iosReadManagedVariables()
        {
            _iosManagedVariablesPath = _rootFolderAbsPath + kIOSUXMPlistPath;
            _iosManagedVariables = loadManagedVariablesFromPlist(_iosManagedVariablesPath);

            if(!File.Exists(_iosManagedVariablesPath))
                Debug.Log("iOS UXM Plist file not found at " + _iosManagedVariablesPath + ". A new file will be created when you click Save.");
        }

        private void iosSaveManagedVariables()
        {
            // Save to iOS
            var plist = new PlistDocument();
            var variableList = new List<PlistElement>();

            if(_iosManagedVariables != null && _iosManagedVariables.Count > 0)
            {
                foreach(var variable in _iosManagedVariables)
                {
                    var plistDict = new PlistElementDict();

                    try
                    {
                        plistDict[kManagedVariableTagKey] = new PlistElementString(variable.Name);
                        plistDict[kManagedVariableDescriptionKey] = new PlistElementString(variable.Description);

                        if(variable.AttributeType == SerializableTypes.Bool)
                        {
                            plistDict[kManagedVariableTypeKey] = new PlistElementString("boolean");
                            plistDict.SetBoolean(kManagedVariableDefaultValueKey, bool.Parse(variable.StringValue));
                        }
                        else if(variable.AttributeType == SerializableTypes.Int)
                        {
                            plistDict[kManagedVariableTypeKey] = new PlistElementString("integer");
                            plistDict.SetInteger(kManagedVariableDefaultValueKey, int.Parse(variable.StringValue));
                            plistDict.SetInteger(kManagedVariableMinKey, (int)variable.Min);
                            plistDict.SetInteger(kManagedVariableMaxKey, (int)variable.Max);
                        }
                        else if(variable.AttributeType == SerializableTypes.Float)
                        {
                            plistDict[kManagedVariableTypeKey] = new PlistElementString("float");
                            plistDict.SetFloat(kManagedVariableDefaultValueKey, float.Parse(variable.StringValue));
                            plistDict.SetFloat(kManagedVariableMinKey, variable.Min);
                            plistDict.SetFloat(kManagedVariableMaxKey, variable.Max);
                        }
                        else if(variable.AttributeType == SerializableTypes.String)
                        {
                            plistDict[kManagedVariableTypeKey] = new PlistElementString("string");
                            plistDict.SetString(kManagedVariableDefaultValueKey, variable.StringValue);
                        }
                    }
                    catch(Exception e)
                    {
                        // catch any parse errors
                        Debug.LogError("Failed to write iOS User Attribute " + variable.Name + ": " + e);
                    }

                    variableList.Add(plistDict);
                }
            }

            plist.root = new PlistElementArray() { values = variableList };
            plist.WriteToFile(_iosManagedVariablesPath);
        }

        #endregion

        #region iOS Extensions Manipulation

        private void iosApplyExtensionSettings()
        {
            //Note: Plist manipulation and PostProcess logic is in UpsightPostProcess.cs
            UpsightPrefs.IosMediationEnabled = _iosMediationEnabled;
            UpsightPrefs.IosPushEnabled = _iosPushEnabled;

            try
            {
                //Mark all files in NativeLibs/iOS/ for ios use
                var iosFiles = Directory.GetFiles(_rootFolderAbsPath + kIOSFolderPath);
                for(int i = 0; i < iosFiles.Length; i++)
                    SetNativeFileActiveState(iosFiles[i], BuildTarget.iOS, true);

                //Mark all frameworks in NativeLibs/iOS/ for ios use
                var iosFrameworks = Directory.GetDirectories(_rootFolderAbsPath + kIOSFolderPath, "*.framework", SearchOption.AllDirectories);
                for(int i = 0; i < iosFrameworks.Length; i++)
                    SetNativeFileActiveState(iosFrameworks[i], BuildTarget.iOS, true);

                //Mark all files in NativeLibs/iOS/<version>/ for ios use
                var currentVersionFiles = Directory.GetFiles(_rootFolderAbsPath + kIOSCurrentVersionPath);
                for(int i = 0; i < currentVersionFiles.Length; i++)
                    SetNativeFileActiveState(currentVersionFiles[i], BuildTarget.iOS, true);

                //Set all mediation framework files
                var mediationFrameworks = Directory.GetDirectories(_rootFolderAbsPath + kIOSCurrentVersionPath, kIOSMediationLib);
                for(int i = 0; i < mediationFrameworks.Length; i++)
                    SetNativeFileActiveState(mediationFrameworks[i], BuildTarget.iOS, _iosMediationEnabled);
            }
            catch(Exception e)
            {
                Debug.LogWarning("Upsight SDK: Error while modifying iOS Ad Mediation Library. Exception: " + e);
            }
        }

        #endregion

        #region Android Data Manipulation

        private void androidReadManifest()
        {
            if(!Directory.Exists(Application.dataPath + kAndroidPluginsDataPath))
                Directory.CreateDirectory(Application.dataPath + kAndroidPluginsDataPath);

            if(!File.Exists(Application.dataPath + kAndroidPluginsDataPath + kAndroidPluginsDataProjectPropertiesName))
                File.WriteAllText(Application.dataPath + kAndroidPluginsDataPath + kAndroidPluginsDataProjectPropertiesName, kAndroidPluginsDataProjectPropertiesContents);

            // The ${applicationId} macro in AndroidManifest.xml is now modified by JarResolver.

            // User Attributes
            _androidManifestPath = Application.dataPath + kAndroidPluginsDataPath + kAndroidManifestPath;
            if(!File.Exists(_androidManifestPath))
            {
                var templatePath = _rootFolderAbsPath + kAndroidManifestTemplatePath;

                if(!File.Exists(templatePath))
                {
                    Debug.LogError("Android Manifest Template File not found at " + templatePath + ". Please reimport the Upsight SDK.");
                    _androidManifestPath = null;
                    return;
                }

                Debug.Log("Android Manifest File not found at " + _androidManifestPath + ". A new file will be created when you click Save.");
                return;
            }


            var xmlDoc = new XmlDocument();
            xmlDoc.Load(_androidManifestPath);

            var metadataElements = xmlDoc.GetElementsByTagName("meta-data");
            foreach(XmlElement element in metadataElements)
            {
                var name = element.GetAttribute(kAndroidXmlElementName);
                var value = element.GetAttribute(kAndroidXmlElementValue);

                if(name == kManifestAppTokenName)
                {
                    _androidAppToken = value;
                }
                else if(name == kManifestPublicKeyName)
                {
                    _androidPublicKey = value;
                }
                else if(name == kManifestGcmName)
                {
                    var gcmParts = value.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);
                    _androidGcmSenderId = gcmParts.Last();
                }
                else if(name.StartsWith(kManifestUserAttributeNamePrefix))
                {
                    var attrName = name.Replace(kManifestUserAttributeNamePrefix, string.Empty);

                    _androidUserAttributes.Add(new UserAttribute(attrName, value));
                }
            }
        }

        private void androidSaveManifest()
        {
            if(string.IsNullOrEmpty(_androidManifestPath))
                return;

            var templatePath = _rootFolderAbsPath + kAndroidManifestTemplatePath;

            string template = File.ReadAllText(templatePath);

            // replace any locations that require the package name
            var xmlString = template
                .Replace(kPackageNameKey, AndroidBundleId)
                .Replace(kPluginVersionKey, UNITY_PLUGIN_VERSION);

            // load up the Manifest Template to use as a base for our changes
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);

            // fill in all our data
            var metadataElements = xmlDoc.GetElementsByTagName("meta-data");
            foreach(XmlElement element in metadataElements)
            {
                var name = element.GetAttribute(kAndroidXmlElementName);

                if(name == kManifestAppTokenName)
                    element.SetAttribute(kAndroidXmlElementValue, _androidAppToken);
                else if(name == kManifestPublicKeyName)
                    element.SetAttribute(kAndroidXmlElementValue, _androidPublicKey);
                else if(name == kManifestGcmName)
                    element.SetAttribute(kAndroidXmlElementValue, AndroidBundleId + "." + _androidGcmSenderId);
            }

            if(_androidUserAttributes != null && _androidUserAttributes.Count > 0)
            {
                // now we grab all the Manifest.xml pieces that we need to add to the AndroidManifest.xml file
                var appElement = xmlDoc.GetElementsByTagName("application")[0];

                foreach(var attribute in _androidUserAttributes)
                {
                    var metaDataNode = xmlDoc.CreateElement("meta-data");
                    appElement.AppendChild(metaDataNode);

                    var nameAttr = xmlDoc.CreateAttribute("android", "name", kAndroidNamespaceUri);
                    nameAttr.Value = kManifestUserAttributeNamePrefix + attribute.TypeName + "." + attribute.Name;
                    metaDataNode.Attributes.Append(nameAttr);

                    var valueAttr = xmlDoc.CreateAttribute("android", "value", kAndroidNamespaceUri);
                    if(attribute.AttributeType == SerializableTypes.DateTime)
                        valueAttr.Value = attribute.StringValue.TrimEnd('Z');
                    else if(attribute.AttributeType == SerializableTypes.Bool)
                        valueAttr.Value = attribute.StringValue.ToLower();
                    else
                        valueAttr.Value = attribute.StringValue;
                    metaDataNode.Attributes.Append(valueAttr);
                }
            }

            xmlDoc.Save(_androidManifestPath);
        }

        #endregion

        #region Android Managed Variables Manipulation

        private void androidReadManagedVariables()
        {
            _androidManagedVariablesPath = Application.dataPath + kAndroidPluginsDataPath + kAndroidUXMJsonDirPath + kAndroidUXMJsonName;
            _androidManagedVariables = loadManagedVariablesFromJson(_androidManagedVariablesPath);

            if(!File.Exists(_androidManagedVariablesPath))
                Debug.Log("Android UXM JSON file not found at " + _androidManagedVariablesPath + ". A new file will be created when you click Save.");
        }

        private void androidSaveManagedVariables()
        {
            var variableList = new List<Dictionary<string, object>>();
            foreach(ManagedVariable variable in _androidManagedVariables)
            {
                var dict = new Dictionary<string, object>();
                dict[kManagedVariableTagKey] = variable.Name;
                dict[kManagedVariableDescriptionKey] = variable.Description;

                if(variable.AttributeType == SerializableTypes.String)
                {
                    dict[kManagedVariableTypeKey] = "string";
                    dict[kManagedVariableDefaultValueKey] = variable.StringValue;
                }
                else if(variable.AttributeType == SerializableTypes.Bool)
                {
                    dict[kManagedVariableTypeKey] = "boolean";
                    dict[kManagedVariableDefaultValueKey] = bool.Parse(variable.StringValue);
                }
                else if(variable.AttributeType == SerializableTypes.Int)
                {
                    dict[kManagedVariableTypeKey] = "integer";
                    dict[kManagedVariableDefaultValueKey] = int.Parse(variable.StringValue);
                    dict[kManagedVariableMinKey] = (int)variable.Min;
                    dict[kManagedVariableMaxKey] = (int)variable.Max;
                }
                else if(variable.AttributeType == SerializableTypes.Float)
                {
                    dict[kManagedVariableTypeKey] = "float";
                    dict[kManagedVariableDefaultValueKey] = float.Parse(variable.StringValue);
                    dict[kManagedVariableMinKey] = variable.Min;
                    dict[kManagedVariableMaxKey] = variable.Max;
                }

                variableList.Add(dict);
            }

            string jsonString = UpsightMiniJSON.Json.Serialize(variableList);

            //Create the res/raw directory if it doesn't exist
            if(!Directory.Exists(Application.dataPath + kAndroidPluginsDataPath + kAndroidUXMJsonDirPath))
                Directory.CreateDirectory(Application.dataPath + kAndroidPluginsDataPath + kAndroidUXMJsonDirPath);

            File.WriteAllText(_androidManagedVariablesPath, jsonString);
        }

        #endregion

        #region Android Extensions Manipulation

        private void androidApplyExtensionSettings()
        {
            UpsightPrefs.AndroidMarketingEnabled = _androidMarketingEnabled;
            UpsightPrefs.AndroidMediationEnabled = _androidMediationEnabled;
            UpsightPrefs.AndroidPushEnabled = _androidPushEnabled;

            try
            {
                var allFiles = Directory.GetFiles(_rootFolderAbsPath + kAndroidLibrariesPath);

                setAndroidExtensionActiveState(allFiles, "analytics", "aar", true);
                setAndroidExtensionActiveState(allFiles, "core", "aar", true);
                setAndroidExtensionActiveState(allFiles, "google-advertising-id", "aar", _androidMarketingEnabled);
                setAndroidExtensionActiveState(allFiles, "google-push-services", "aar", _androidPushEnabled);
                setAndroidExtensionActiveState(allFiles, "managed-variables", "aar", true);
                setAndroidExtensionActiveState(allFiles, "marketing", "aar", _androidMarketingEnabled);
                setAndroidExtensionActiveState(allFiles, "marketplace", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-ac", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-ac-lib", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-al", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-m2", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mopub-sdk-base", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mopub-sdk-interstitial", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mopub-sdk-rewardedvideo", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-u2", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-u2-lib", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-ss", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-vungle", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "android-job", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "cat", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "fetch", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "runtime", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-vast", "aar", _androidMarketingEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-ad-network-mraid", "aar", _androidMarketingEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-common", "aar", _androidMarketingEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-core", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "mediation-lib", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "rxandroid", "aar", true);
                setAndroidExtensionActiveState(allFiles, "unity-binding", "aar", true);
                setAndroidExtensionActiveState(allFiles, "unity-binding-base", "aar", true);
                setAndroidExtensionActiveState(allFiles, "unity-binding-marketing", "aar", _androidMarketingEnabled);
                setAndroidExtensionActiveState(allFiles, "unity-binding-mediation", "aar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "unity-binding-push", "aar", _androidPushEnabled);
                setAndroidExtensionActiveState(allFiles, "volley", "aar", true);

                setAndroidExtensionActiveState(allFiles, "dagger", "jar", true);
                setAndroidExtensionActiveState(allFiles, "gson", "jar", true);
                setAndroidExtensionActiveState(allFiles, "commons-io", "jar", true);
                setAndroidExtensionActiveState(allFiles, "javax.inject", "jar", true);
                setAndroidExtensionActiveState(allFiles, "mopub-volley", "jar", _androidMarketingEnabled);
                setAndroidExtensionActiveState(allFiles, "libAvid-mopub", "jar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "applovin-sdk", "jar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "common", "jar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "converter-gson", "jar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "logging-interceptor", "jar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "okhttp", "jar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "okio", "jar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "retrofit", "jar", _androidMediationEnabled);
                setAndroidExtensionActiveState(allFiles, "otto", "jar", true);
                setAndroidExtensionActiveState(allFiles, "rxjava", "jar", true);

                //Mark .so files in jni/
                if(Directory.Exists(_rootFolderAbsPath + kAndroidLibrariesX86Path))
                {
                    var x86Files = Directory.GetFiles(_rootFolderAbsPath + kAndroidLibrariesX86Path);
                    foreach(var f in x86Files)
                    {
                        SetNativeFileActiveState(f, BuildTarget.iOS, false);
                        SetNativeFileActiveState(f, BuildTarget.Android, true, "x86");
                    }
                }

                if(Directory.Exists(_rootFolderAbsPath + kAndroidLibrariesArmv7Path))
                {
                    var armv7Files = Directory.GetFiles(_rootFolderAbsPath + kAndroidLibrariesArmv7Path);
                    foreach(var f in armv7Files)
                    {
                        SetNativeFileActiveState(f, BuildTarget.iOS, false);
                        SetNativeFileActiveState(f, BuildTarget.Android, true, "ARMv7");
                    }
                }

                AssetDatabase.Refresh();
            }
            catch(Exception e)
            {
                Debug.LogWarning("Upsight SDK: Error while moving Upsight Android Extensions. Exception: " + e);
            }
        }

        private void setAndroidExtensionActiveState(IEnumerable<string> allFiles, string packageName, string extension, bool setActive)
        {
            var regex = new Regex("^" + packageName + @"-\d+(\.\d+)*.*");

            var foundPackages = allFiles.Where(package => regex.IsMatch(FileSystemUtil.GetLastFileOrFolderInPath(package)));

            foreach(string package in foundPackages)
                SetNativeFileActiveState(package, BuildTarget.Android, setActive);

            if(foundPackages.Count() == 0)
                Debug.LogWarning("Could not find Upsight Android Extension " + packageName + ".");
        }

        #endregion

        #region Android Push Icon Manipulation

        private void updateAndroidPushIcon(string iconPath, string iconName)
        {
            try
            {
                var bytes = File.ReadAllBytes(_newIconPath);
                string header = System.Text.Encoding.ASCII.GetString(bytes, 1, 3);
                _icon.LoadImage(bytes);

                //Do some mild verification that this is in fact an image
                if((bytes[0] == 'J' && header == "FIF") || (bytes[0] == (byte)0x89 && header == "PNG"))
                {
                    //Check that the size is correct
                    if(_icon.height == kAndroidPushIconHeight && _icon.width == kAndroidPushIconWidth)
                    {
                        if(!Directory.Exists(iconPath))
                            Directory.CreateDirectory(iconPath);

                        File.WriteAllBytes(iconPath + iconName, _icon.EncodeToPNG());

                        _newIconPath = null;
                        _error = null;
                    }
                    else
                    {
                        _error = "The image is not " + kAndroidPushIconWidth + "x" + kAndroidPushIconHeight + " pixels.";
                    }
                }
                else
                {
                    _error = "File is not a valid image.";
                }
            }
            catch
            {
                _error = "File could not be read.";
            }
        }

        #endregion

        #region Managed Variables Helpers

        private List<ManagedVariable> loadManagedVariablesFromJson(string filePath)
        {
            // Create a new List for the Managed Variables.
            var managedVariables = new List<ManagedVariable>();

            // Assign the file that holds the Managed Variables.
            if(filePath == null || !File.Exists(filePath))
                return managedVariables;

            // Parse the file that holds the Managed Variables.
            var variableList = Json.ToJsonArray(File.ReadAllText(filePath));
            foreach(Dictionary<string, object> dict in variableList)
            {
                // Get the name of the type from the JSON.
                string typeName = dict[kManagedVariableTypeKey].ToString();

                // New Managed Variable Object.
                var managedVariable = new ManagedVariable(
                    typeName + "." + dict[kManagedVariableTagKey].ToString(),
                    dict[kManagedVariableDescriptionKey].ToString(),
                    dict[kManagedVariableDefaultValueKey].ToString());

                if("float" == typeName || "integer" == typeName)
                {
                    try
                    {
                        if(dict.ContainsKey(kManagedVariableMinKey))
                            managedVariable.Min = dict.GetPrimitive<float>(kManagedVariableMinKey);

                        if(dict.ContainsKey(kManagedVariableMaxKey))
                            managedVariable.Max = dict.GetPrimitive<float>(kManagedVariableMaxKey);
                    }
                    catch { }
                }

                // Add to the list.
                managedVariables.Add(managedVariable);
            }

            // Return the created list.
            return managedVariables;
        }

        private List<ManagedVariable> loadManagedVariablesFromPlist(string filePath)
        {
            // Create a new List for the Managed Variables.
            var managedVariables = new List<ManagedVariable>();

            // Assign the file that holds the Managed Variables. Abort if it doesn't exist.
            if(filePath == null || !File.Exists(filePath))
                return managedVariables;

            // Parse the file that holds the Managed Variables.
            var variablePlist = new PlistDocument();
            variablePlist.ReadFromFile(filePath);
            var variableList = variablePlist.root as PlistElementArray;
            foreach(PlistElement plistElement in variableList.values)
            {
                PlistElementDict dict = plistElement.AsDict();

                // Get the default value from the plist.
                var defaultValue = dict[kManagedVariableDefaultValueKey];

                // New Empty Managed Variable Object
                var variable = new ManagedVariable(dict[kManagedVariableTagKey].AsString(), dict[kManagedVariableDescriptionKey].AsString(), defaultValue.ToString());

                if(defaultValue is PlistElementString)
                {
                    variable.AttributeType = SerializableTypes.String;
                }
                else if(defaultValue is PlistElementBoolean)
                {
                    variable.AttributeType = SerializableTypes.Bool;
                }
                else if(defaultValue is PlistElementFloat)
                {
                    variable.AttributeType = SerializableTypes.Float;
                    variable.Min = dict[kManagedVariableMinKey].AsFloat();
                    variable.Max = dict[kManagedVariableMaxKey].AsFloat();
                }
                else if(defaultValue is PlistElementInteger)
                {
                    variable.AttributeType = SerializableTypes.Int;
                    variable.Min = dict[kManagedVariableMinKey].AsInteger();
                    variable.Max = dict[kManagedVariableMaxKey].AsInteger();
                }

                // Add to the list.
                managedVariables.Add(variable);
            }

            // Return the created list.
            return managedVariables;
        }

        #endregion

        #region Validation and Helpers

        /// <summary>
        /// returns false if the attributes have invalid data
        /// </summary>
        /// <returns><c>true</c>, if user attributes was validated, <c>false</c> otherwise.</returns>
        private bool validateUserAttributes()
        {
            return _iosUserAttributes.All(attr => attr.IsValid) && _iosUserAttributes.Distinct().Count() == _iosUserAttributes.Count
                && _androidUserAttributes.All(attr => attr.IsValid) && _androidUserAttributes.Distinct().Count() == _androidUserAttributes.Count;
        }

        private bool validateManagedVariables()
        {
            return _iosManagedVariables.All(mv => mv.IsValid) && _iosManagedVariables.Distinct().Count() == _iosManagedVariables.Count
                && _androidManagedVariables.All(mv => mv.IsValid) && _androidManagedVariables.Distinct().Count() == _androidManagedVariables.Count;
        }

        private static bool ValidatePublicKey(string inputKey)
        {
            try
            {
                //Check that the string is base64 encoded
                var bytes = Convert.FromBase64String(inputKey);

                //RSA keys are encoded using the ASN.1 type system
                //All we will check is that the outermost layer represents an ASN.1 Sequence
                //and that the length of the sequence matches the specified length in the header.
                //The RSA public key itself is 256 bytes so the sequence should be at least that long.
                //While this doesn't actually check that that these is an RSA key in the sequence, this is fine for now.
                if(bytes != null && bytes.Length > 256 && bytes[0] == 0x30 && bytes[1] == 0x82 && bytes.Length == ((bytes[2] << 8) + bytes[3] + 4))
                {
                    return true;
                }
            }
            catch { }
            return false;
        }

        internal static bool IsNativeFileActive(string path, BuildTarget platform)
        {
            if(path.StartsWith(Application.dataPath))
                path = "Assets" + path.Substring(Application.dataPath.Length).Replace('\\', '/');

            var pluginInfo = PluginImporter.GetAtPath(path) as PluginImporter;
            return pluginInfo != null && pluginInfo.GetCompatibleWithPlatform(platform);
        }

        internal static void SetNativeFileActiveState(string path, BuildTarget platform, bool setActive, string targetCpu = null)
        {
            if(path.StartsWith(Application.dataPath))
                path = "Assets" + path.Substring(Application.dataPath.Length).Replace('\\', '/');

            if(path.EndsWith(".meta"))
                return;

            if(!File.Exists(path) && !Directory.Exists(path))
                return;

            var pluginInfo = PluginImporter.GetAtPath(path) as PluginImporter;
            if(pluginInfo != null && (pluginInfo.GetCompatibleWithAnyPlatform() || pluginInfo.GetCompatibleWithPlatform(platform) != setActive))
            {
                pluginInfo.SetCompatibleWithAnyPlatform(false);
                pluginInfo.SetCompatibleWithPlatform(platform, setActive);
                if(targetCpu != null)
                    pluginInfo.SetPlatformData(platform, "CPU", targetCpu);
                pluginInfo.SaveAndReimport();
            }
        }


        //Gets and Sets the BundleID / ApplicationID in Unity's PlayerSettings for Android
        internal static string AndroidBundleId
        {
            get
            {
#if UNITY_5_6_OR_NEWER
                return PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android);
#else
                return PlayerSettings.bundleIdentifier;
#endif
            }

            set
            {
#if UNITY_5_6_OR_NEWER
                PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, value);
#else
                PlayerSettings.bundleIdentifier = value;
#endif
            }
        }


        //Gets and Sets the BundleID / ApplicationID in Unity's PlayerSettings for Ios
        internal static string IosBundleId
        {
            get
            {
#if UNITY_5_6_OR_NEWER
                return PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS);
#else
                return PlayerSettings.bundleIdentifier;
#endif
            }

            set
            {
#if UNITY_5_6_OR_NEWER
                PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, value);
#else
                PlayerSettings.bundleIdentifier = value;
#endif
            }
        }

        #endregion

        #region Internal classes

        //String representations of SerializableTypes, this is how the values will be tagged so that they can be deserialized
        //This array must match the SerializableTypes enum
        public static string[] SerializableTypeStrings = new string[]
        {
            "string",
            "integer",
            "float",
            "boolean",
            "datetime",
        };

        //This represents all the types we are capable of Serializing
        //NOTE: Not all SerializableTypes are valid for all use cases
        //This enum must correspond to SerializableTypeStrings and SerializableTypes, the int values must match SerializableTypes
        public enum SerializableTypes
        {
            String = 0,
            Int = 1,
            Float = 2,
            Bool = 3,
            DateTime = 4,
        }

        //This represents the valid types for UserAttributes
        //NOTE: Not all SerializableTypes are valid UserAttributes types
        //This enum must correspond to SerializableTypeStrings and SerializableTypes, the int values must match SerializableTypes
        enum UserAttributeTypes
        {
            String = 0,
            Int = 1,
            Float = 2,
            Bool = 3,
            DateTime = 4,
        }

        //This represents the valid types for ManagedVariables
        //NOTE: Not all SerializableTypes are valid ManagedVariable types
        //This enum must correspond to TypeStrings and SerializableTypes, the int values must match SerializableTypes
        public enum ManagedVariableTypes
        {
            String = 0,
            Int = 1,
            Float = 2,
            Bool = 3,
        }

        public class UserAttribute : IEqualityComparer<UserAttribute>
        {
            public UserAttribute(string name, string value)
            {
                //Check to see if the name is in the form <type>.<name> and if the type is valid use that as the type
                var parts = name.Split(new char[] { '.' }, 2);
                int type = Array.FindIndex(SerializableTypeStrings, s => s == parts[0]);
                if(parts.Length == 2 && type >= 0 && type < SerializableTypeStrings.Length)
                {
                    //Valid type
                    Name = parts[1];
                    AttributeType = (SerializableTypes)type;
                }
                else
                {
                    //Type not found, use the whole original value as the name and infer the type
                    Name = name;
                    AttributeType = InferTypeFromString(value);
                }

                StringValue = value;
            }

            //Required by ReorderableLists, will create an invalid UserAttribute
            public UserAttribute() : this("", "")
            {
                _valueValid = false;
                _nameValid = false;
            }

            protected string _name;
            public string Name
            {
                get { return _name; }
                set
                {
                    if(value != _name)
                    {
                        _name = value;
                        _nameValid = !string.IsNullOrEmpty(_name);
                    }
                }
            }

            protected string _value;
            public string StringValue
            {
                get { return _value; }
                set
                {
                    if(value != _value)
                    {
                        _value = value;
                        ValidateValue();
                    }
                }
            }

            protected SerializableTypes _type;
            public SerializableTypes AttributeType
            {
                get { return _type; }
                set
                {
                    if(value != _type)
                    {
                        _type = value;
                        ValidateValue();
                    }
                }
            }

            protected bool _nameValid;
            protected bool _valueValid;
            virtual public bool IsValid
            {
                get { return _nameValid && _valueValid; }
            }

            public string TypeName
            {
                get { return SerializableTypeStrings[(int)AttributeType]; }
            }

            protected virtual void ValidateValue()
            {
                try
                {
                    switch(_type)
                    {
                        case SerializableTypes.Bool:
                            bool.Parse(_value);
                            break;
                        case SerializableTypes.DateTime:
                            if(string.IsNullOrEmpty(_value))
                            {
                                _value = kDefaultDate;
                            }
                            else if(_value[_value.Length - 1] != 'Z')
                            {
                                DateTime.ParseExact(_value, "yyyy-MM-ddTHH:mm:ss", null);
                                _value += "Z";
                            }
                            else
                            {
                                DateTime.ParseExact(_value, "yyyy-MM-ddTHH:mm:ssZ", null);
                            }
                            break;
                        case SerializableTypes.Float:
                            float.Parse(_value);
                            break;
                        case SerializableTypes.Int:
                            int.Parse(_value);
                            break;
                        case SerializableTypes.String:
                        default:
                            break;
                    }
                    _valueValid = true;
                }
                catch
                {
                    _valueValid = false;
                }
            }

            private static SerializableTypes InferTypeFromString(string value)
            {
                //If there is not type specified we can try to infer it
                //Used for legacy manifest format, therefore will never set type to DateTime
                float f;
                int i;

                if(value.Equals("true", StringComparison.OrdinalIgnoreCase) || value.Equals("false", StringComparison.OrdinalIgnoreCase))
                    return SerializableTypes.Bool;
                else if(int.TryParse(value, out i))
                    return SerializableTypes.Int;
                else if(float.TryParse(value, out f))
                    return SerializableTypes.Float;
                else
                    return SerializableTypes.String;
            }

            public bool Equals(UserAttribute x, UserAttribute y)
            {
                return (x.Name == null && y.Name == null) || (x.Name != null && x.Name.Equals(y.Name));
            }

            public int GetHashCode(UserAttribute obj)
            {
                return obj.Name.GetHashCode();
            }
        }

        public class ManagedVariable : UserAttribute
        {
            public ManagedVariable(string name, string description, string value) : base(name, value)
            {
                _description = description;
            }

            //Required by ReorderableLists, will create an invalid ManagedVariable
            public ManagedVariable() : this("", "", "") { }

            private string _description;
            public string Description
            {
                get { return _description; }
                set
                {
                    if(value != _description)
                    {
                        _description = value;
                    }
                }
            }

            private float _min;
            public float Min
            {
                get { return _min; }
                set
                {
                    if(value != _min && (AttributeType == SerializableTypes.Int || AttributeType == SerializableTypes.Float))
                    {
                        _min = value;
                        ValidateValue();
                    }
                }
            }

            private float _max;
            public float Max
            {
                get { return _max; }
                set
                {
                    if(value != _max && (AttributeType == SerializableTypes.Int || AttributeType == SerializableTypes.Float))
                    {
                        _max = value;
                        ValidateValue();
                    }
                }
            }

            protected override void ValidateValue()
            {
                if(AttributeType == SerializableTypes.Int)
                {
                    int value;
                    _valueValid = int.TryParse(_value, out value) && Min <= value && value <= Max;
                }
                else if(AttributeType == SerializableTypes.Float)
                {
                    float value;
                    _valueValid = float.TryParse(_value, out value) && Min <= value && value <= Max;
                }
                else
                {
                    base.ValidateValue();
                }
            }

            public override bool IsValid
            {
                get { return base.IsValid && !string.IsNullOrEmpty(Description); }
            }
        }

        #endregion

        #region Reorderable list helpers

        private void setupLists()
        {
            _iosReorderableList = new ReorderableList(_iosUserAttributes, typeof(UserAttribute), false, true, true, true);
            SetupReorderableList("iOS", _iosReorderableList, _iosUserAttributes);

            _androidReorderableList = new ReorderableList(_androidUserAttributes, typeof(UserAttribute), false, true, true, true);
            SetupReorderableList("Android", _androidReorderableList, _androidUserAttributes);

            _iosManagedVariablesReorderableList = new ReorderableList(_iosManagedVariables, typeof(ManagedVariable), false, true, true, true);
            SetupManagedVariablesReorderbleList("iOS", _iosManagedVariablesReorderableList, _iosManagedVariables);

            _androidManagedVariablesReorderableList = new ReorderableList(_androidManagedVariables, typeof(ManagedVariable), false, true, true, true);
            SetupManagedVariablesReorderbleList("Android", _androidManagedVariablesReorderableList, _androidManagedVariables);
        }

        private static void SetupReorderableList(string platform, ReorderableList reorerableList, List<UserAttribute> elementList)
        {
            reorerableList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                DrawElement(elementList[index], rect, index, isActive, isFocused, platform);
            };
            reorerableList.drawHeaderCallback = (Rect rect) =>
            {
                EditorGUI.LabelField(rect, platform + " User Attributes");
            };
            reorerableList.onRemoveCallback = OnRemoveItem;
            reorerableList.elementHeight = 10 + EditorGUIUtility.singleLineHeight;
        }

        private static void SetupManagedVariablesReorderbleList(string platform, ReorderableList reorerableList, List<ManagedVariable> elementList)
        {
            reorerableList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                DrawManagedVariableElement(elementList[index], rect, index, isActive, isFocused);
            };
            reorerableList.drawHeaderCallback = (Rect rect) =>
            {
                EditorGUI.LabelField(rect, platform + " Managed Variables");
            };
            reorerableList.onRemoveCallback = OnRemoveItem;
            reorerableList.elementHeight = 13 + EditorGUIUtility.singleLineHeight * 2;
        }

        private static void DrawElement(UserAttribute attr, Rect rect, int index, bool isActive, bool isFocused, string platform)
        {
            rect.y += 4;

            var kElementSpace = 3;
            var kEnumWidth = 70;
            var kValueWidth = 150;
            var kNameWidth = rect.width - kEnumWidth - kValueWidth - (3 * kElementSpace);
            var xPos = rect.x;

            Color validColor = GUI.color;

            attr.AttributeType = (SerializableTypes)EditorGUI.EnumPopup(new Rect(xPos, rect.y, kEnumWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, (UserAttributeTypes)attr.AttributeType);
            xPos += kEnumWidth + kElementSpace;

            GUI.color = string.IsNullOrEmpty(attr.Name) ? Color.red : validColor;
            attr.Name = EditorGUI.TextField(new Rect(xPos, rect.y, kNameWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, attr.Name);
            xPos += kNameWidth + kElementSpace;

            GUI.color = attr.IsValid ? validColor : Color.red;
            attr.StringValue = EditorGUI.TextField(new Rect(xPos, rect.y, kValueWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, attr.StringValue);

            GUI.color = validColor;
        }

        private static void DrawManagedVariableElement(ManagedVariable variable, Rect rect, int index, bool isActive, bool isFocused)
        {
            rect.y += 4;

            var kElementSpace = 3;
            var kEnumWidth = 50;
            var kTagWidth = 140;
            var kValueWidth = 140;
            var kMinWidth = 50;
            var kMaxWidth = 50;
            var xPos = rect.x;
            var yPos = rect.y;

            Color validColor = GUI.color;

            variable.AttributeType = (SerializableTypes)EditorGUI.EnumPopup(new Rect(xPos, rect.y, kEnumWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, (ManagedVariableTypes)variable.AttributeType);
            xPos += kEnumWidth + kElementSpace;

            GUI.color = string.IsNullOrEmpty(variable.Name) ? Color.red : validColor;
            variable.Name = EditorGUI.TextField(new Rect(xPos, rect.y, kTagWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, variable.Name);
            xPos += kTagWidth + kElementSpace;

            GUI.color = variable.IsValid ? validColor : Color.red;
            variable.StringValue = EditorGUI.TextField(new Rect(xPos, rect.y, kValueWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, variable.StringValue);
            xPos += kValueWidth + kElementSpace;

            if(variable.AttributeType == SerializableTypes.Int)
            {
                variable.Min = EditorGUI.IntField(new Rect(xPos, rect.y, kMinWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, (int)variable.Min);
                xPos += kMinWidth + kElementSpace;
                variable.Max = EditorGUI.IntField(new Rect(xPos, rect.y, kMaxWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, (int)variable.Max);
            }
            else if(variable.AttributeType == SerializableTypes.Float)
            {
                variable.Min = EditorGUI.FloatField(new Rect(xPos, rect.y, kMinWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, variable.Min);
                xPos += kMinWidth + kElementSpace;
                variable.Max = EditorGUI.FloatField(new Rect(xPos, rect.y, kMaxWidth, EditorGUIUtility.singleLineHeight), GUIContent.none, variable.Max);
            }

            GUI.color = string.IsNullOrEmpty(variable.Description) ? Color.red : validColor;
            yPos += EditorGUIUtility.singleLineHeight + kElementSpace;
            variable.Description = EditorGUI.TextField(new Rect(rect.x, yPos, rect.width, EditorGUIUtility.singleLineHeight), GUIContent.none, variable.Description);

            GUI.color = validColor;
        }

        private static void OnRemoveItem(ReorderableList l)
        {
            if(EditorUtility.DisplayDialog("Warning!", "Are you sure you want to delete the user attribute?", "Yes", "No"))
            {
                ReorderableList.defaultBehaviours.DoRemoveButton(l);
            }
        }

        #endregion
    }

    //Bitmask enum with dependencies
    public enum UpsightAndroidExtensions
    {
        Mediation = 1 << 2 | Marketing,
        Push = 1 << 1 | Marketing,
        Marketing = 1 << 0,
    }

    public enum UpsightIosExtensions
    {
        Mediation = 1 << 2,
        Push = 1 << 1,
        //Marketing = 1 << 0, Unused but reserved
    }
}
