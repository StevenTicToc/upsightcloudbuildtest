/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


/// <summary>
/// this script is part of the demo scene. It is designed so that you can just stick it on a GO in your scene then call any Upsight
/// methods. It will log every event that occurs making it very useful in learning how the plugin operates.
/// </summary>
public class UpsightEventListener : MonoBehaviour
{
#if UNITY_IOS || UNITY_ANDROID
    void OnEnable()
    {
        // Listen to all events for illustration purposes
        UpsightManager.sessionDidStartEvent += sessionDidStartEvent;
        UpsightManager.sessionDidResumeEvent += sessionDidResumeEvent;
        UpsightManager.userSessionDidStartEvent += userSessionDidStartEvent;
        UpsightManager.userSessionDidResumeEvent += userSessionDidResumeEvent;
        UpsightManager.managedVariablesDidSynchronizeEvent += managedVariablesDidSynchronizeEvent;
        UpsightManager.onBillboardAppearEvent += onBillboardAppearEvent;
        UpsightManager.onBillboardDismissEvent += onBillboardDismissEvent;
        UpsightManager.billboardDidReceivePurchaseEvent += billboardDidReceivePurchaseEvent;
        UpsightManager.billboardDidReceiveRewardEvent += billboardDidReceiveRewardEvent;
        UpsightManager.billboardDidReceiveDataEvent += billboardDidReceiveDataEvent;
        UpsightManager.onContentAvailableEvent += onContentAvailable;
        UpsightManager.onContentNotAvailableEvent += onContentNotAvailable;
        UpsightManager.onPartnerInitializedEvent += onPartnerInitialized;
    }


    void OnDisable()
    {
        // Remove all event handlers
        UpsightManager.sessionDidStartEvent -= sessionDidStartEvent;
        UpsightManager.sessionDidResumeEvent -= sessionDidResumeEvent;
        UpsightManager.userSessionDidStartEvent -= userSessionDidStartEvent;
        UpsightManager.userSessionDidResumeEvent -= userSessionDidResumeEvent;
        UpsightManager.managedVariablesDidSynchronizeEvent -= managedVariablesDidSynchronizeEvent;
        UpsightManager.onBillboardAppearEvent -= onBillboardAppearEvent;
        UpsightManager.onBillboardDismissEvent -= onBillboardDismissEvent;
        UpsightManager.billboardDidReceivePurchaseEvent -= billboardDidReceivePurchaseEvent;
        UpsightManager.billboardDidReceiveRewardEvent -= billboardDidReceiveRewardEvent;
        UpsightManager.billboardDidReceiveDataEvent -= billboardDidReceiveDataEvent;
        UpsightManager.onContentAvailableEvent -= onContentAvailable;
        UpsightManager.onContentNotAvailableEvent -= onContentNotAvailable;
        UpsightManager.onPartnerInitializedEvent -= onPartnerInitialized;
    }




    void sessionDidStartEvent()
    {
        Debug.Log("sessionDidStartEvent");
    }


    void sessionDidResumeEvent()
    {
        Debug.Log("sessionDidResumeEvent");
    }


    void userSessionDidStartEvent()
    {
        Debug.Log("userSessionDidStartEvent");
    }


    void userSessionDidResumeEvent()
    {
        Debug.Log("userSessionDidResumeEvent");
    }


    void managedVariablesDidSynchronizeEvent(List<string> tags)
    {
        if(null == tags)
        {
            Debug.Log("managedVariablesDidSynchronizeEvent: received null tags list");
            return;
        }

        StringBuilder sb = new StringBuilder();
        foreach(string tag in tags)
        {
            sb.Append("\n").Append(tag);
        }
        Debug.Log("managedVariablesDidSynchronizeEvent:" + sb.ToString());
    }


    void onBillboardAppearEvent(string scope, UpsightContentAttributes content)
    {
        Debug.Log("onBillboardAppearEvent: scope=" + scope + ", content=" + content);
    }


    void onBillboardDismissEvent(string scope)
    {
        Debug.Log("billboardDidDismissEvent: " + scope);
    }


    void billboardDidReceivePurchaseEvent(UpsightPurchase purchase)
    {
        Debug.Log("billboardDidReceivePurchaseEvent: " + purchase);
    }


    void billboardDidReceiveRewardEvent(UpsightReward reward)
    {
        Debug.Log("billboardDidReceiveRewardEvent: " + reward);
    }


    private void billboardDidReceiveDataEvent(UpsightData data)
    {
        Debug.Log("billboardDidReceiveDataEvent: " + data);
    }

    private void onContentAvailable(string scope, Dictionary<string, string> data)
    {
        Debug.Log("onContentAvailable: scope=" + scope);
    }

    private void onContentNotAvailable(string scope, Dictionary<string, string> data)
    {
        Debug.Log("onContentNotAvailable: scope=" + scope);
    }

    private void onPartnerInitialized(string partnerName)
    {
        Debug.Log("onPartnerInitialized: partner=" + partnerName);
    }

#endif
}
