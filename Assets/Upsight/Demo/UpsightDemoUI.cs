/*
 *  Copyright (C) 2017 Upsight. All rights reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;

public class UpsightDemoUI : MonoBehaviour
{
#if UNITY_IOS || UNITY_ANDROID
    [HideInInspector]
    public string _customScope = "morepre";

    public Image BackgroundPanel;
    public Image OfferImage;
    public Image OrnamentImage;
    public Text ButtonText;

    private bool _shouldSynchronizeManagedVariables = true;
    private string _isContentReady = "";

    private bool _showingNativeMessage = false;
    private Texture2D _texture;
    private UpsightData _upsightData;

    void Start()
    {
        Upsight.init();
    }

    void OnEnable()
    {
        UpsightManager.onBillboardAppearEvent += onBillboardAppearEvent;
        UpsightManager.onBillboardDismissEvent += onBillboardDismissEvent;
        UpsightManager.billboardDidReceiveDataEvent += billboardDidReceiveDataEvent;
        UpsightManager.onContentAvailableEvent += onContentAvailable;
        UpsightManager.onContentNotAvailableEvent += onContentNotAvailable;
        UpsightManager.onPartnerInitializedEvent += onPartnerInitialized;
    }


    void OnDisable()
    {
        UpsightManager.onBillboardAppearEvent -= onBillboardAppearEvent;
        UpsightManager.onBillboardDismissEvent -= onBillboardDismissEvent;
        UpsightManager.billboardDidReceiveDataEvent -= billboardDidReceiveDataEvent;
        UpsightManager.onContentAvailableEvent -= onContentAvailable;
        UpsightManager.onContentNotAvailableEvent -= onContentNotAvailable;
        UpsightManager.onPartnerInitializedEvent -= onPartnerInitialized;
    }

    void OnGUI()
    {
        if(_showingNativeMessage)
            return;

        beginGuiColomn();

        if(GUILayout.Button("Enable Verbose Logs"))
        {
            Upsight.setLoggerLevel(UpsightLoggerLevel.Verbose);
        }


        if(GUILayout.Button("Log Config Data"))
        {
            Debug.Log("plugin version: " + Upsight.getPluginVersion());
            Debug.Log("app token: " + Upsight.getAppToken());
            Debug.Log("public key: " + Upsight.getPublicKey());
            Debug.Log("sender ID: " + Upsight.getSid());
        }


        GUILayout.Space(20);
        if(GUILayout.Button("Set User Attributes"))
        {
            Upsight.setUserAttributeString("name", "mary");
            Upsight.setUserAttributeInt("age", 14);
            Upsight.setUserAttributeBool("isAlive", true);
            Upsight.setUserAttributeFloat("distance", 26.7f);
        }


        if(GUILayout.Button("Get User Attributes"))
        {
            Debug.Log("name: " + Upsight.getUserAttributeString("name"));
            Debug.Log("age: " + Upsight.getUserAttributeInt("age"));
            Debug.Log("isAlive: " + Upsight.getUserAttributeBool("isAlive"));
            Debug.Log("distance: " + Upsight.getUserAttributeFloat("distance"));
        }


        if(GUILayout.Button("Set Opt-Out Status"))
        {
            Upsight.setOptOutStatus(true);
        }


        if(GUILayout.Button("Get Opt-Out Status"))
        {
            Debug.Log("opt-out status: " + Upsight.getOptOutStatus());
        }


        if(GUILayout.Button("Set Location"))
        {
            Upsight.setLocation(51.792, 4.6307);
        }


        GUILayout.Space(20);
        if(GUILayout.Button("Report Custom Event"))
        {
            var dict = new Dictionary<string, object>();
            dict.Add("first_key", "first_value");
            dict.Add("second_key", 38);

            Upsight.recordCustomEvent("my-event", dict);
        }


        if(GUILayout.Button("Record IAP Event"))
        {
            Upsight.recordGooglePlayPurchase(2, "USD", 1.99, 1.99, "com.upsight.product1", 0, "{\"orderId\":\"12999763169054705758.1371079406387615\",\"packageName\":\"com.example.app\",\"productId\":\"exampleSku\",\"purchaseTime\":1345678900000,\"purchaseState\":0,\"developerPayload\":\"bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ\",\"purchaseToken\":\"opaque-token-up-to-1000-characters\"}", "mockSignature");
            Upsight.recordAppleStorePurchase(2, "USD", 1.99, "com.upsight.product1", "some product", UpsightPurchaseResolution.Buy);
        }


        if(GUILayout.Button("Record Monetization Event"))
        {
            Upsight.recordMonetizationEvent(2.99, "USD", UpsightPurchaseResolution.Buy, "product-monetization");
        }


        bool theToggleValue = GUILayout.Toggle(_shouldSynchronizeManagedVariables, "Should sync UXM values");
        if(theToggleValue != _shouldSynchronizeManagedVariables)
        {
            _shouldSynchronizeManagedVariables = theToggleValue;
            Upsight.setShouldSynchronizeManagedVariables(_shouldSynchronizeManagedVariables);
        }

        var managedBool = Upsight.getManagedBool("enable_extended_mode").ToString();
        var managedString = Upsight.getManagedString("name");
        var managedInt = Upsight.getManagedInt("coins").ToString();
        var managedFloat = Upsight.getManagedFloat("strength").ToString();

        GUILayout.Label("enable_extended_mode: " + managedBool);
        GUILayout.Label("name: " + managedString);
        GUILayout.Label("coins: " + managedInt);
        GUILayout.Label("strength: " + managedFloat);

        endGuiColumn(true);


        if(GUILayout.Button("Register for Push Notifications"))
        {
            Upsight.registerForPushNotifications();
        }


        if(GUILayout.Button("Unregister for Push Notifications"))
        {
            Upsight.unregisterForPushNotifications();
        }

        GUILayout.Space(20);



        if(GUILayout.Button("Prepare Billboard (rewcl)"))
        {
            Upsight.prepareBillboard("rewcl");
        }


        if(GUILayout.Button("Record Milestone (rewcl)"))
        {
            Upsight.recordMilestoneEvent("rewcl");
        }


        if(GUILayout.Button("Destroy Billboard (rewcl)"))
        {
            Upsight.destroyBillboard("rewcl");
        }


        GUILayout.Space(20);
        if(GUILayout.Button("Prepare Billboard (vgpcl)"))
        {
            Upsight.prepareBillboard("vgpcl");
        }


        if(GUILayout.Button("Record Milestone (vgpcl)"))
        {
            Upsight.recordMilestoneEvent("vgpcl");
        }


        if(GUILayout.Button("Destroy Billboard (vgpcl)"))
        {
            Upsight.destroyBillboard("vgpcl");
        }


        GUILayout.Space(20);

        GUILayout.BeginHorizontal();
        {
            GUILayout.Label("Custom Scope", GUILayout.Width(Screen.width / 4));
            _customScope = GUILayout.TextField(_customScope, GUILayout.Width(Screen.width / 4));
        }
        GUILayout.EndHorizontal();

        if(GUILayout.Button("Prepare Billboard (" + _customScope + ")"))
        {
            Upsight.prepareBillboard(_customScope);
        }


        if(GUILayout.Button("Record Milestone (" + _customScope + ")"))
        {
            Upsight.recordMilestoneEvent(_customScope);
        }


        if(GUILayout.Button("Destroy Billboard (" + _customScope + ")"))
        {
            Upsight.destroyBillboard(_customScope);
        }

        GUILayout.Label(_isContentReady);

        endGuiColumn();
    }

    void onBillboardAppearEvent(string scope, UpsightContentAttributes content)
    {
    }

    void onBillboardDismissEvent(string scope)
    {
    }

    private void onContentAvailable(string scope, Dictionary<string, string> data)
    {
        _isContentReady = "Content became ready for " + scope;
    }

    private void onContentNotAvailable(string scope, Dictionary<string, string> data)
    {
        _isContentReady = "Content not available for " + scope;
    }

    private void onPartnerInitialized(string partnerName)
    {
    }

    private void billboardDidReceiveDataEvent(UpsightData data)
    {
        _upsightData = data;

        BackgroundPanel.color = data.GetColor("BGColor");

        ButtonText.text = data.GetString("BodyCopy");

        Texture2D offerTexture = new Texture2D(1, 1);
        offerTexture.LoadImage(File.ReadAllBytes(data.GetImage("OfferImage").ImagePath));
        OfferImage.sprite = Sprite.Create(offerTexture, new Rect(0, 0, offerTexture.width, offerTexture.height), Vector2.zero);

        Texture2D ornamentTexture = new Texture2D(1, 1);
        ornamentTexture.LoadImage(File.ReadAllBytes(data.GetImage("OrnamentImage").ImagePath));
        OrnamentImage.sprite = Sprite.Create(ornamentTexture, new Rect(0, 0, ornamentTexture.width, ornamentTexture.height), Vector2.zero);

        _showingNativeMessage = true;
        BackgroundPanel.gameObject.SetActive(true);
    }

    public void nativeMessageDismissed()
    {
        BackgroundPanel.gameObject.SetActive(false);
        _showingNativeMessage = false;
        _upsightData.RecordClickEvent();
        _upsightData.RecordDismissEvent();
        _upsightData.Destroy();
        _upsightData = null;
    }

    #region GUI Helpers

    void beginGuiColomn()
    {
        var buttonHeight = (Screen.width >= 960 || Screen.height >= 960) ? 70 : 30;

        GUI.skin.label.margin = new RectOffset(0, 0, 10, 0);
        GUI.skin.label.alignment = TextAnchor.MiddleCenter;
        GUI.skin.button.margin = new RectOffset(0, 0, 10, 0);
        GUI.skin.button.fixedHeight = buttonHeight;
        GUI.skin.button.fixedWidth = Screen.width / 2 - 20;
        GUI.skin.button.wordWrap = true;
        GUI.skin.button.fontSize = buttonHeight / 3;
        GUI.skin.toggle.fixedHeight = buttonHeight;
        GUI.skin.toggle.fixedWidth = Screen.width / 2 - 20;
        GUI.skin.toggle.fontSize = buttonHeight / 3;
        GUI.skin.toggle.alignment = TextAnchor.MiddleLeft;
        GUI.skin.textField.fontSize = buttonHeight / 2;
        GUI.skin.textField.fixedWidth = Screen.width / 2 - 20;
        GUI.skin.label.fontSize = buttonHeight / 3;
        GUI.skin.label.alignment = TextAnchor.MiddleLeft;
        GUI.skin.label.margin = new RectOffset(5, 5, 0, 0);

        GUILayout.BeginArea(new Rect(10, 10, Screen.width / 2, Screen.height));
        GUILayout.BeginVertical();
    }


    void endGuiColumn(bool hasSecondColumn = false)
    {
        GUILayout.EndVertical();
        GUILayout.EndArea();

        if(hasSecondColumn)
        {
            GUILayout.BeginArea(new Rect(Screen.width - Screen.width / 2, 10, Screen.width / 2, Screen.height));
            GUILayout.BeginVertical();
        }
    }

    #endregion

#endif
}
